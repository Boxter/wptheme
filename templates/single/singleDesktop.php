<?php

use Carbon\Carbon;


while (have_posts()) : the_post();
    $categories = get_the_category();
    $catName = $categories[0]->name;
    $dotMetricsId = getDotMetricsId($categories[0]->slug);
    $catLink = str_replace('/category', '', get_category_link($categories[0]->term_id));
    $multipleAuthors = new GfWpPluginContainer\Wp\MultipleAuthors\MultipleAuthors();
    $postOwners = $multipleAuthors->getOwnersForPost(get_the_ID());

    $blocks = count(parse_blocks($post->post_content)) > 0 ? parse_blocks($post->post_content) : [];
    $lead = '';

    if (in_array($blocks[0]['blockName'], ['core/heading', 'core/paragraph'])) {
        $lead = $blocks[0]['innerHTML'];
        $lead = str_replace(['<h2>', '</h2>'], '', $lead);
        unset($blocks[0]);
    }

    $renderedBlocks = [];
    foreach ($blocks as $key => $block) {
        //new format for galleries
        if ($block['blockName'] === 'core/gallery') {
            $ids = implode(',', $block['attrs']['ids']);
            $ligthboxSize = '1500x1500';
            if (wp_is_mobile()) {
                $ligthboxSize = 'single';
                foreach ($block['attrs']['ids'] as $id) {
                    $imageData = wp_get_attachment_metadata($id);
                    if ($imageData['width'] < $imageData['height']){
                        $ligthboxSize = 'portrait-m';
                    }
                }
            }
            $renderedBlocks[] = do_shortcode(sprintf('[gallery ids="%s" lightbox_max_size="%s"]', $ids, $ligthboxSize));
            continue;
        }
        //sets sizes of images for caption image
        if ($block['blockName'] === 'core/image') {
            $imageCaption = mb_strtoupper(wp_get_attachment_caption($block['attrs']['id']));

            if(strlen($imageCaption) && substr($imageCaption,0,4)!=='FOTO') {
                $imageCaption = 'FOTO: ' . $imageCaption;
            }


            $imageLegenda = get_post_meta($block['attrs']['id'], 'legenda', true);
            $imageData = wp_get_attachment_metadata($block['attrs']['id']);

            if (wp_is_mobile()) {
                $imageSize = 'list-big';
                $imageWidth = '427';
                $imageHeight = '285';
                if ($block['attrs']['sizeSlug'] === 'full') {
                    $imageSize = 'landsape-m';
                    $imageWidth = '400';
                    $imageHeight = '200';
                } else if (isset($imageData['width'],$imageData['height']) && $imageData['width'] < $imageData['height']) {
                    $imageSize = 'portrait-m';
                    $imageWidth = '400';
                    $imageHeight = '800';
                }
            } else {
                $imageSize = 'single';
                $imageWidth = '872';
                $imageHeight = '610';
                if (isset($imageData['width'],$imageData['height']) && $imageData['width'] < $imageData['height']){
                   $imageSize = 'portrait';
                    $imageWidth = '800';
                    $imageHeight = '1200';
                } else if (isset($block['attrs']['sizeSlug']) && $block['attrs']['sizeSlug'] === 'full') {
                    $imageSize = 'full';
                    $imageWidth = $imageData['width'];
                    $imageHeight = $imageData['height'];
                }
            }

            $imageUrl = wp_get_attachment_image_url($block['attrs']['id'], $imageSize);
            $block = '<figure class="captionImageWrapper"><img src="' . $imageUrl . '" alt="' . esc_attr($imageCaption) . '" width="' . $imageWidth . '" height="' . $imageHeight . '" />
        <figcaption class="captionImageCaption">' . $imageCaption . '</figcaption>
        </figure>
        <span class="keySingle">' . $imageLegenda . '</span>';
        } else {
            $block = apply_filters('the_content', render_block($block));
            $block = str_replace(['<br/>', '<br />', '<br>'], '', $block);

            // detect lead type ( check for h2, h3, h4 ) if not found then extract first <p> tag
            if (!strlen($lead)) {
                $start = mb_strpos($block, '<p>');
                $end = mb_strpos($block, '</p>', $start);
                $lead = mb_substr($block, $start, $end - $start + 4);
            }
        }

        $renderedBlocks[] = str_replace($lead, '', $block);
    }

    $featuredImageId = get_post_thumbnail_id($post->ID);

    if(!$featuredImageId){
        $featuredImageId = get_option("defaultFeaturedImage");
    }

    if (wp_is_mobile()) {
        $imageUrl = esc_url(wp_get_attachment_image_url($featuredImageId, 'list-big'));
        $imageWidth = 427;
        $imageHeight = 285;
    } else {
        $imageUrl = esc_url(wp_get_attachment_image_url($featuredImageId, 'single'));
        $imageWidth = 872;
        $imageHeight = 610;
    }
    $imageLegenda = get_post_meta($featuredImageId, 'legenda', true);
    $title = $post->post_title;

    // Post dates

    $publishedDate = new Carbon($post->post_date);
    $publishedDateString = str_replace(['May', 'Aug', 'Oct'], ['Maj', 'Avg', 'Okt'], $publishedDate->format('d.M,Y.'));

//    $publishedDate->locale('sr'); // does not work ???
//    Carbon::setLocale('sr');
//    echo strftime('%d.%h,%G.', strtotime($publishedDate->toDateTimeString())) . PHP_EOL;
//    echo $publishedDate->formatLocalized('%d.%h,%G.');
    $updatedDate = new Carbon($post->post_modified);

    $featuredImageCaption = mb_strtoupper(get_the_post_thumbnail_caption($post->ID));

    if(strlen($featuredImageCaption) && substr($featuredImageCaption,0,4)!=="FOTO"){
        $featuredImageCaption = "FOTO: " . $featuredImageCaption;
    }

    ?>
    <article class="article">
    <header class="article__top">
        <div class="article__top-content">
            <a class="categoryPageNameLink" href="<?= $catLink ?>" title="<?= $catName ?>"><?= $catName ?></a>
            <h1><?= $title ?></h1>
            <p><?= strip_tags($lead, '<strong><h2><h3><h4>'); ?></p>
        </div>
        <figure class="singleFeaturedImageWrapper">
            <img src="<?= $imageUrl ?>" alt="<?= esc_attr($title) ?>" width="<?= $imageWidth ?>" height="<?= $imageHeight ?>"/>
            <figcaption><?= $featuredImageCaption ?></figcaption>
        </figure>
        <div class="keyFeatured">
            <span><?= $imageLegenda ?></span>
        </div>
    </header>
    <?php
    if (wp_is_mobile()) {
        dynamic_sidebar('category_feed_baner_mobile_1');
    }
    ?>
    <div class="article__center">
        <section class="article__left">
            <aside class="article__aside-left">
                <div class="author">
                    <?php if (count($postOwners) > 1 && count($postOwners) !== 0): ?>
                    <span>Autori:</span>
                    <?php else:?>
                    <span>Autor:</span>
                    <?php endif;
                    $i = 1;
                    $comma = ',';
                    $count = count($postOwners);
                    /** @var \GfWpPluginContainer\Wp\MultipleAuthors\Model\GfPostOwner $postOwner */
                    foreach($postOwners as $postOwner):
                    if ($i === count($postOwners)) {
                        $comma = '';
                    }
                        $i++;
                        $ownerLink = get_author_posts_url($postOwner->getAuthorId());
                        $ownerDisplayName = $postOwner->getAuthorDisplayName();
                    ?>
                        <a href="<?=$ownerLink?>" title="<?=$ownerDisplayName?>"><?=$ownerDisplayName?></a><?=$comma?>
                    <?php endforeach; ?>
                </div>
                <time datetime="<?= $publishedDate->toDateTimeString() ?>">
                    <div class="date"><?= $publishedDateString ?> </div>
                    <div class="time">
                        <i class="fas fa-circle"></i>
                        <?= $publishedDate->toTimeString('minute') ?>
                        <?php
                        if ($publishedDate->format('Y/m/d/H:i') < $updatedDate->format('Y/m/d/H:i')) :?>
                            <i class="fas fa-arrow-right"></i>
                            <?= $updatedDate->toTimeString('minute') ?>
                        <?php endif; ?>
                    </div>
                </time>
                <div class="social">
                    <?php dynamic_sidebar('post_social_sidebar'); ?>
                </div>
            </aside>
            <div class="article__content">
                <?php
                $bannerPosition = 1;
                $validBlock = 1;
                foreach ($renderedBlocks as $key => $block) {
                    if ($validBlock === 1 && strlen($block)) {
                        $block = html_entity_decode($block, ENT_COMPAT, 'UTF-8');
                        $block = str_replace_once(html_entity_decode('&ndash;', ENT_COMPAT, 'UTF-8'), '', $block);
                        $block = str_replace_once(html_entity_decode('&mdash;', ENT_COMPAT, 'UTF-8'), '', $block);
                    }

                    if ($validBlock === 1 && isset($block[4]) && (in_array($block[4], ['&', '-']))) {
                        $block = str_replace_once(['&#8211;', '-'], '', $block);
                    }
                    echo $block;

                    if (strlen($block)) {
                        $validBlock++;
                    }

                    if ($validBlock % 5 === 0) {
                        if (wp_is_mobile()) {
                            dynamic_sidebar('single_intext_feed_baner_mobile_' . $bannerPosition);
                        } else {
                            dynamic_sidebar('single_intext_feed_baner_' . $bannerPosition);
                        }
                        $bannerPosition++;
                    }
                } ?>
                <div class="article__tags">
                    <a href="<?= $catLink ?>" title="<?= $catName ?>" rel="category"><?= $catName ?></a>
                    <?= the_tags('', '', '') ?>
                </div>
                <?php if (wp_is_mobile()) {
                    dynamic_sidebar('single_post_bottom_banner_mobile');
                } else {
                    dynamic_sidebar('single_post_bottom_banner');
                } ?>
            </div>
            <footer class="article__bottom">
                <div class="article__comments container">
                    <div class="container">
                        <p>Možete da nas pratite i na Facebook stranici:</p>
                        <div class="fb__page" style="display: block; margin: auto;"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="fb-comments" data-href="<?= get_permalink(get_the_ID()) ?>" data-numposts="5"
                         data-width="">
                    </div>
                </div>

                <?php if (wp_is_mobile()) {
                    dynamic_sidebar('single_post_bottom_mobile');
                } else {
                    dynamic_sidebar('single_post_bottom');
                } ?>
            </footer>
        </section>

        <aside class="article__right">
            <?php if (wp_is_mobile()) {
                dynamic_sidebar('single_page_sidebar_mobile');
            } else {
                dynamic_sidebar('single_page_sidebar_desk');
            } ?>
        </aside>
    </div>

<?php endwhile; ?>
    </article>
<?php if (!is_user_logged_in()): ?>
    <script>
        jQuery(document).ready(function (){
            setAjaxViewCount()
        })
        function setAjaxViewCount(){
            jQuery.ajax({
                url: "<?=admin_url(). 'admin-ajax.php'?>",
                type: 'POST',
                data: "action=setAjaxViewCount&postId=<?=$post->ID?>",
                success: function (response) {
                },
                error: function () {
                }
            });
        }
    </script>
<?php endif ?>
<?php
function str_replace_once($str_pattern, $str_replacement, $string)
{
    if (!is_string($str_pattern) && !is_int($str_pattern)) {
        return $string;
    }
    $occurrence = strpos($string, $str_pattern);
    if ($occurrence !== false) {
        return substr_replace($string, $str_replacement, $occurrence, strlen($str_pattern));
    }

    return $string;
}