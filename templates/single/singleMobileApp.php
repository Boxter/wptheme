<?php

use Carbon\Carbon;

/**
 * currently set in banners.php. @TODO refactor :)
 */
global $appSinglePost;

$multipleAuthors = new GfWpPluginContainer\Wp\MultipleAuthors\MultipleAuthors();
$postOwners = $multipleAuthors->getOwnersForPost($appSinglePost->ID);
?>
    <article class="article">
        <?php
        global $dotMetricsId;
        $category = get_category(wp_get_post_categories($appSinglePost->ID)[0]);
        $catName = $category->name;
        $socialPermalink = get_permalink($appSinglePost->ID);
        $dotMetricsId = getDotMetricsId($category->slug);
        $catLink = str_replace('/category', '', get_category_link($category->term_id));
        $postTags = wp_get_post_tags($appSinglePost->ID);
        $blocks = count(parse_blocks($appSinglePost->post_content)) > 0 ? parse_blocks($appSinglePost->post_content) : [];
        $lead = '';
        // If there is a heading block, get the first one

        if (in_array($blocks[0]['blockName'], ['core/heading', 'core/paragraph'])) {
            $lead = $blocks[0]['innerHTML'];
            $lead = str_replace(['<h2>', '</h2>'], '', $lead);
            unset($blocks[0]);
        }
        $renderedBlocks = [];
        foreach ($blocks as $key => $block) {
            //new format for galleries
            if ($block['blockName'] === 'core/gallery') {
                $ids = implode(',', $block['attrs']['ids']);
                $ligthboxSize = '1536x1536';
                if (wp_is_mobile()) {
                    $ligthboxSize = 'large';
                    foreach ($block['attrs']['ids'] as $id) {
                        $imageData = wp_get_attachment_metadata($id);
                        if ($imageData['width'] < $imageData['height']) {
                            $ligthboxSize = 'portrait-m';
                        }
                    }
                }
                $renderedBlocks[] = do_shortcode(sprintf('[gallery ids="%s" lightbox_max_size="%s"]', $ids,
                    $ligthboxSize));
                continue;
            }

            if ($block['blockName'] === 'core/image') {

                $imageCaption = mb_strtoupper(wp_get_attachment_caption($block['attrs']['id']));


                if (strlen($imageCaption) && substr($imageCaption, 0, 4) !== 'FOTO') {
                    $imageCaption = 'FOTO: ' . $imageCaption;
                }


                $imageSize = 'list-big';
                $imageData = wp_get_attachment_metadata($block['attrs']['id']);
                $imageWidth = '427';
                $imageHeight = '285';
                if (isset($block['attrs']['sizeSlug']) && $block['attrs']['sizeSlug'] === 'full') {
                    $imageSize = 'landsape-m';
                    $imageWidth = '400';
                    $imageHeight = '200';
                } else if (isset($imageData['width'],$imageData['height']) && $imageData['width'] < $imageData['height']) {
                    $imageSize = 'portrait-m';
                    $imageWidth = '400';
                    $imageHeight = '800';
                }

                $imageUrl = wp_get_attachment_image_url($block['attrs']['id'], $imageSize);
                $imageLegenda = get_post_meta($block['attrs']['id'], 'legenda', true);
                $legendaTag = '';
                $block = '<figure class="captionImageWrapper"><img src="' . $imageUrl . '" alt="' . esc_attr($imageCaption) . '" width="' . $imageWidth . '" height="' . $imageHeight . '" />
        <figcaption class="captionImageCaption">' . $imageCaption . $legendaTag . '</figcaption>
        </figure>
        <span class="keySingle">' . $imageLegenda . '</span>';
            } else {
                $block = apply_filters('the_content', render_block($block));
                $block = str_replace(['<br/>', '<br />', '<br>'], '', $block);

                // detect lead type ( check for h2, h3, h4 ) if not found then extract first <p> tag
                if (!strlen($lead)) {
                    $start = mb_strpos($block, '<p>');
                    $end = mb_strpos($block, '</p>', $start);
                    $lead = mb_substr($block, $start, $end - $start + 4);
                }
            }
            $renderedBlocks[] = str_replace($lead, '', $block);
        }

        $imageUrl = esc_url(wp_get_attachment_image_url((get_post_thumbnail_id($appSinglePost->ID) ?: get_option("defaultFeaturedImage")),
            'list-big'));
        $imageWidth = 427;
        $imageHeight = 285;
        // Post dates
        $publishedDate = new Carbon($appSinglePost->post_date);
        $publishedDateString = str_replace(['May', 'Aug', 'Oct'], ['Maj', 'Avg', 'Okt'],
            $publishedDate->format('d.M,Y.'));
        $updatedDate = new Carbon($appSinglePost->post_modified);
        $title = $appSinglePost->post_title;

        $featuredImageId = get_post_thumbnail_id($appSinglePost->ID);
        $imageLegenda = get_post_meta($featuredImageId, 'legenda', true);
        $featuredImageCaption = mb_strtoupper(get_the_post_thumbnail_caption($appSinglePost->ID));


        if (strlen($featuredImageCaption) && substr($featuredImageCaption, 0, 4) !== "FOTO") {
            $featuredImageCaption = "FOTO: " . $featuredImageCaption;
        }
        ?>
        <header class="article__top">
            <div class="article__top-content">
                <h2>
                    <a class="categoryPageNameLink" title="<?= $catName ?>"
                       href="<?= parseAppUrl('page', $catLink) ?>"><?= $catName ?></a>
                </h2>
                <h1><?= $title ?></h1>
                <p><?= strip_tags($lead, '<strong><h2><h3><h4>'); ?></p>
            </div>
            <figure>
                <img src="<?= $imageUrl ?>"
                     alt="<?= esc_attr($appSinglePost->post_title) ?>"/>
                <figcaption><?= $featuredImageCaption ?></figcaption>
            </figure>
            <div class="keyFeatured">
                <span><?= $imageLegenda ?></span>
            </div>
        </header>
        <?php
        dynamic_sidebar('category_feed_baner_mobile_1');
        ?>
        <div class="article__center">
            <section class="article__left">
                <aside class="article__aside-left">
                    <div class="author">
                        <?php if (count($postOwners) > 1 && count($postOwners) !== 0): ?>
                            <span>Autori:</span>
                        <?php else:?>
                        <span>Autor:</span>
                        <?php endif;
                        $i = 1;
                        $comma = ',';
                        $count = count($postOwners);
                        /** @var \GfWpPluginContainer\Wp\MultipleAuthors\Model\GfPostOwner $postOwner */
                        foreach($postOwners as $postOwner):
                            if ($i === $count) {
                                $comma = '';
                            }
                            $i++;
                        $ownerLink = parseAppUrl('author', get_author_posts_url($postOwner->getAuthorId()));
                        $ownerDisplayName = $postOwner->getAuthorDisplayName();
                        ?>
                        <a href="<?=$ownerLink?>" title="<?=$ownerDisplayName?>"><?=$ownerDisplayName?></a><?=$comma?>
                        <?php endforeach; ?>
                    </div>
                    <time datetime="<?= $publishedDate->toDateTimeString() ?>">
                        <div class="date"><?= $publishedDateString ?> </div>
                        <div class="time">
                            <i class="fas fa-circle"></i>
                            <?= $publishedDate->toTimeString('minute') ?>
                            <?php
                            if ($publishedDate->format('Y/m/d/h:i') < $updatedDate->format('Y/m/d/h:i')) :?>
                                <i class="fas fa-arrow-right"></i>
                                <?= $updatedDate->toTimeString('minute') ?>
                            <?php endif; ?>
                        </div>
                    </time>
                    <script src="https://cdn.jsdelivr.net/npm/sharer.js@latest/sharer.min.js"></script>
                    <div class="social socialApp">
                        <button class="button" data-sharer="facebook" data-url="<?= $socialPermalink ?>"><i
                                    class="fa fa-facebook"></i></button>
                        <button class="button" data-sharer="twitter" data-title="<?= $title ?>"
                                data-url="<?= $socialPermalink ?>"><i class="fa fa-twitter"></i></button>
                        <a class="mobileAppShare" href="viber://forward?text=<?= $socialPermalink ?>"><i
                                    class="fab fa-viber"></i></a>
                        <a class="mobileAppShareWhatsApp"
                           href="https://wa.me/?text=<?= rawurlencode($socialPermalink) ?>"><i
                                    class="fab fa-whatsapp"></i></a>
                        <i class="fas fa-share-alt" id="uachange"
                           onclick="location.href='shareto://<?= $socialPermalink ?>';"></i>
                    </div>
                </aside>
                <div class="article__content">
                    <?php
                    $bannerPosition = 1;
                    $validBlock = 1;
                    foreach ($renderedBlocks as $key => $block) {
                        if ($validBlock === 1 && strlen($block)) {
                            $block = html_entity_decode($block, ENT_COMPAT, 'UTF-8');
                            $block = str_replace_once(html_entity_decode('&ndash;', ENT_COMPAT, 'UTF-8'), '', $block);
                            $block = str_replace_once(html_entity_decode('&mdash;', ENT_COMPAT, 'UTF-8'), '', $block);
                        }

                        if ($validBlock === 1 && isset($block[4]) && (in_array($block[4], ['&', '-']))) {
                            $block = str_replace_once(['&#8211;', '-'], '', $block);
                        }
                        echo $block;

                        if (strlen($block)) {
                            $validBlock++;
                        }

                        if ($validBlock % 5 === 0) {
                            dynamic_sidebar('single_intext_feed_baner_mobile_' . $bannerPosition);
                            $bannerPosition++;
                        }
                    } ?>
                    <div class="article__tags">
                        <a href="<?= parseAppUrl('page', $catLink) ?>" title="<?= $catName ?>"><?= $catName ?></a>
                        <?php /** @var WP_Term $tag */
                        foreach ($postTags as $tag): ?>
                            <a href="<?= parseAppUrl('tag', get_tag_link($tag)) ?>" rel="tag"
                               title="<?= esc_attr($tag->slug) ?>"><?= $tag->name ?></a>
                        <?php endforeach; ?>
                    </div>
                    <?php
                    dynamic_sidebar('single_post_bottom_banner_mobile');
                    ?>
                </div>
            </section>

            <aside class="article__right">
                <?php dynamic_sidebar('single_page_sidebar_mobile'); ?>
            </aside>
        </div>

        <footer class="article__bottom">
            <div class="article__comments">
                <div class="container">
                    <p>Možete da nas pratite i na Facebook stranici:</p>
                    <div class="fb__page" style="width: 340px;display: block; margin: auto;"></div>
                </div>
            </div>
            <div class="container">
                <div class="fb-comments" data-href="<?= $socialPermalink ?>" data-numposts="5" data-width=""></div>
            </div>
            <?php
            dynamic_sidebar('single_post_bottom_mobile');
            ?>
        </footer>
    </article>
<?php if (!is_user_logged_in()): ?>
    <script>
        jQuery(document).ready(function () {
            setAjaxViewCount()
        })
        function setAjaxViewCount() {
            jQuery.ajax({
                url: "<?=admin_url() . 'admin-ajax.php'?>",
                type: 'POST',
                data: "action=setAjaxViewCount&postId=<?=$post->ID?>",
                success: function (response) {
                },
                error: function () {
                }
            }
    </script>
<?php endif ?>
<?php
function str_replace_once($str_pattern, $str_replacement, $string)
{
    if (strpos($string, $str_pattern) !== false) {
        $occurrence = strpos($string, $str_pattern);

        return substr_replace($string, $str_replacement, strpos($string, $str_pattern), strlen($str_pattern));
    }

    return $string;
}