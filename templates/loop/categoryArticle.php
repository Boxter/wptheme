<?php $class = $infiniteScroll ? 'infScroll': ''?>
<article class="news__item <?=$class?>">
    <a title="<?=esc_attr($title)?>" href="<?=$permalink?>">
        <figure><img class="lazy-loaded" data-lazy-type="image" data-src="<?=$post['imageUrl']?>" alt="<?=esc_attr($title)?>" height="<?=$post['imageHeight']?>" width="<?=$post['imageWidth']?>" src=""></figure>
    </a>
    <div>
        <span class="news__category">
            <span class="categoryAuthor">
                 <?php if (mb_strtoupper($catName)=='KOLUMNE' ) : ?>
                     <a  title="<?=$authorName?>" href="<?=$authorUrl?>"><?=$authorName?></a>
                 <?php else : ?>
                     <a title="<?=$catName?>" href="<?=$catUrl?>"><?=$catName?></a>
                 <?php endif;?>

            </span>
            <span class="postDate"><i class="fa fa-clock"></i> <?= $timeAgo[0]?> <?= $timeAgo[1] ?></span>
        </span>

        <h2><a title="<?=esc_attr($title)?>" href="<?=$permalink?>"><?=$title?></a></h2>
        <p><?= parseHeading($blocks)?></p>
    </div>
</article>