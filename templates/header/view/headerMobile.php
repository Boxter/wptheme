<header class="header">
    <div class="header__top">
        <div class="menu">
                <span class="menu__button">
                    <i class="fas fa-bars"></i>
                    <i class="fas fa-search"></i>
                </span>
        </div>

		<?php if ( ! strpos( $_SERVER['REQUEST_URI'], 'mobile-home' ) !== false ) : ?>
            <div class="backButtonWrapper">
                <img id="bckbtn" onclick="window.history.back()"
                     src="<?= PARENT_THEME_DIR_URI . '/images/back-button.svg' ?>"
                     alt="back button"
                     title="back button"
                     style="height: 33px;width: 33px">
            </div>
            <script>
                if (history.length < 2) {
                    document.getElementById("bckbtn").setAttribute("onClick", "window.location.href='https://srpskainfo.com/mobile-home/'");

                    window.history.pushState("", "", "#");
                    window.addEventListener("popstate", function () {

                        history.replaceState(null, document.title, window.location.pathname);
                        setTimeout(function () {
                            window.location.replace("http://srpskainfo.com/mobile-home/");
                        }, 0);

                    }, false);
                }
            </script>
		<?php endif; ?>

		<?php if ( in_array( $_SERVER['REQUEST_URI'], [ '/mobile-home/', '/?isapp=true' ] ) ) {
			$onclick = "window.location.reload(true)";
		} else {
			$onclick = "window.location.href='" . get_home_url() . "/mobile-home'";
		} ?>

        <div class="logo" onclick="<?= $onclick ?>">
            <a class="custom-logo-link" title="<?= get_bloginfo( 'name', 'display' ) ?>" rel="home">
                <img class="custom-logo" src="<?= get_theme_mod( 'gf_logo' ) ?>" alt="<?= get_bloginfo( 'name' ) ?>" width="180" height="33">
            </a>
        </div>
        <div class="reloadButtonWrapper">
            <img onclick="window.location.reload(true)" src="<?= PARENT_THEME_DIR_URI . '/images/reload.svg' ?>"
                 alt="reload"
                 title="reload"
                 style="height: 33px;width: 33px">
        </div>
    </div>
</header>
<main>
