<?php
// Set the width and image to be used in <img> width and height attributes. We only use two cases, desktop and mobile
$logoWidth = '320';
$logoHeight = '59';
if (wp_is_mobile()) {
    $logoWidth = '180';
    $logoHeight = '40';
}
?>
<header class="header">
    <div class="header__top">
        <div class="menu">
            <span class="menu__button">
                <i class="fas fa-bars"></i>
                <i class="fas fa-search"></i>
            </span>
        </div>
        <div class="logo">
            <a class="custom-logo-link" title="<?=get_bloginfo( 'name', 'display' )?>" rel="home" href="<?= get_home_url()?>">
                <img class="custom-logo" src="<?=get_theme_mod('gf_logo')?>" alt="<?= get_bloginfo('name') ?>" width="<?=$logoWidth?>" height="<?=$logoHeight?>">
            </a>
        </div>
        <div class="social">
            <ul>
                <?php dynamic_sidebar('top_bar_right'); ?>
            </ul>
        </div>
    </div>
    <div class="header__bottom">
        <div class="container">
            <nav>
                <ul>
                <?php
                $key = 'navigation-desktop#Main Navigation';
                $html = $cache->get($key);
                if ($html === false) {
                    $menu = wp_get_nav_menu_items('Main Navigation');
                    $specialCategory = null;
                    $html = '';
                    /** @var WP_Post $menuItem */
                    foreach ($menu as $menuItem) {
                        $html .= '<li><a title="'. esc_attr($menuItem->title) .'" href="'. $menuItem->url.  '">'. $menuItem->title .'</a></li>';
                    }
                    $cache->set($key, $html, 300);
                }
                echo $html;
                /**
                 * This category has a logo instead of text in nav menu and it has to be last
                 *
                 */
                ?>
                    <li class="sponsor">
                        <a title="Digitalna Srpska" href="/digitalna-srpska/"><img src="<?=get_theme_mod('gf_side_logo')?>" alt="Digitalna Srpska" title="Digitalna Srpska" width="80" height="60">
                        <div class="sponsor__logo"><span>By</span> <img src="<?= CHILD_THEME_DIR_URI . '/images/logo-mtel.svg' ?>" alt="By Mtel" width="40" height="40"/></div>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<main>