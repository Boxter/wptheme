<?php
$url = $data['link'];
$tip = $data['tip'];

$class = '';

if ($tip === 'video') {
    $class = 'fb-video';
}

if ($tip === 'slika') {
    $class = 'fb-post';
}
?>

<div style="margin-bottom: 15px;" data-href="<?=$url?>" class="<?=$class?>" data-width="550"
     data-show-text="false"></div>
