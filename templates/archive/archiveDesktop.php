<?php use Carbon\Carbon;
/*
 * When infinite scroll function is triggered this is set to true and its used to change class
 *  of items printed via infinite scroll
 */
$infiniteScroll = false;

dynamic_sidebar( 'category_page_top_banner' ); ?>
<!-- Container Start -->
<div class="container">
    <!-- siCategory Start -->
    <div class="siCategory">
        <div class="category__title">
        <?php if (isset($searchQuery)): ?>
            <h1><?= sprintf('Rezultati pretrage za: %s', $searchQuery) ?></h1>
        <?php endif ?>
        <?php if ( is_author() ): ?>
            <h1><?= get_the_author() ?></h1>
        <?php endif ?>
        <?php if (isset($catName)) : ?>
            <h1><span>#</span><?=ucfirst($catName);?></h1>
        <?php endif ?>
        </div>
        <div class="category__left">
            <!-- Category Left Side Start -->
            <section class="news items__2">
                <?php
                    $lastPage = ceil( $sortedItems['totalCount'] / PER_PAGE);
                    if ( count( $sortedItems['articles'] ) ) {
                        /* @var \GfWpPluginContainer\Wp\Article $article */
                        $bannerPosition = 1;
                        foreach ( $sortedItems['articles'] as $key => $article ) {
                            $post = [];
                            $blocks       = count(parse_blocks($article->getBody())) > 0 ? parse_blocks($article->getBody()) : null;
                            $permalink    = esc_url($article->getPermalink());
                            $thumbnailSrc = esc_url($article->getThumbnail());
                            $featuredImageUrl=(get_post_thumbnail_id($article->getPostId()) ? : get_option("defaultFeaturedImage"));

                            if (wp_is_mobile()) {
                                if ($key < 1) {
                                    $post['imageUrl'] = esc_url(wp_get_attachment_image_url($featuredImageUrl, 'large'));
                                    $post['imageWidth'] = 374;
                                    $post['imageHeight'] = 250;
                                } else {
                                    $post['imageUrl'] = esc_url(wp_get_attachment_image_url($featuredImageUrl, 'medium'));
                                    $post['imageWidth'] = 120;
                                    $post['imageHeight'] = 82;
                                }

                            } else {
                                $post['imageUrl'] = esc_url(wp_get_attachment_image_url($featuredImageUrl, 'list-big'));
                                $post['imageWidth'] = 427;
                                $post['imageHeight'] = 285;
                            }
                            $title        = esc_html($article->getTitle());
                            $catName = mb_strtolower($article->getCategory()['name']);
                            $catUrl = '/' . $article->getCategory()['slug'] . '/';
                            // If the category name is kolumne get the author information and display it in the template
                            if ($catName === 'kolumne') {
                                $postId = $article->getPostId();
                                $authors = $article->getAuthor();
                                $authorName = $article->getAuthor()[0]['name'];
                                $authorUrl = get_author_posts_url($article->getAuthor()[0]['id']);
                            }
                            Carbon::setLocale('bs');
                            $postDate = new Carbon($article->getPublishedAt(), new \DateTimeZone('Europe/Sarajevo'));
                            $timeAgo = explode(' ', $postDate->longAbsoluteDiffForHumans());
                            include(__DIR__ . '/../loop/categoryArticle.php');

                            if (wp_is_mobile()) {
                                if (($key+1) % 3 === 0) {
                                    dynamic_sidebar('category_feed_baner_mobile_' . $bannerPosition);
                                    $bannerPosition++;
                                }
                            } else {
                                if (($key+1) % 4 === 0) {
                                    dynamic_sidebar('category_feed_baner_' . $bannerPosition);
                                    $bannerPosition++;
                                }
                            }
                        }
                    } ?>
            </section>
            <div class="siPagination">
                <?php \GfWpPluginContainer\Pagination\Pagination::dottedPagination( $lastPage, $paged ); ?>
            </div>
        </div><!-- Category Left Side End  -->

        <aside class="category__right"> <!-- Category Right Side -->
            <?php if (wp_is_mobile()) {
                dynamic_sidebar('category_page_sidebar_mobile');
            } else {
                dynamic_sidebar('category_page_sidebar');
            } ?>
        </aside><!-- Category Right Side End -->
    </div><!-- siCategory End -->
</div><!-- Container End -->