</main>
<footer class="footer">
    <div class="container">
        <div class="footer__top">
            <div class="footer__menu">
                <h3>Vijesti</h3>
                <ul>
                    <?php
                        $menu = wp_get_nav_menu_items('Footer Menu Left');
                    foreach ($menu as $menuItem) :?>
                        <li><a href="<?= parseAppUrl('page', $menuItem->url)?>" title="<?= $menuItem->title ?>"><?= $menuItem->title ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <ul>
                    <?php
                    $menu = wp_get_nav_menu_items('Footer Menu Center');
                    foreach ($menu as $menuItem) :?>
                        <li><a href="<?= parseAppUrl('page', $menuItem->url) ?>" title="<?= $menuItem->title ?>"><?= $menuItem->title ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <ul>
                    <?php
                    $menu = wp_get_nav_menu_items('Footer Menu Right');
                    foreach ($menu as $menuItem) :?>
                        <li><a href="<?= parseAppUrl('page', $menuItem->url)?>" title="<?= $menuItem->title ?>"><?= $menuItem->title ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="footer__social">
                <h3>Društvene mreže</h3>
                <ul>
                    <?php dynamic_sidebar("footer_bar_right"); ?>
                </ul>
            </div>
        </div>

        <div class="footer__bottom">
            <nav>
                <ul>
                    <?php
                    $menu = wp_get_nav_menu_items('Footer Menu Bottom');
                    foreach ($menu as $menuItem) :?>
                        <li><a href="<?= parseAppUrl('page', $menuItem->url) ?>" title="<?= $menuItem->title ?>"><?= $menuItem->title ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </nav>
            <span>© Srpska info EUROBLIC <?php echo date("Y");?></span>
        </div>
    </div>
</footer>