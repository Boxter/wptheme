<?php

// default category page

if (USE_ELASTIC !== false) {
    header('Location: ' . str_replace('category/', '', home_url($_SERVER['REQUEST_URI'])));
    exit();
}
