<?php
/* Template Name: application endpoint */

use \GfWpPluginContainer\Indexer\Repository\Article as ArticleRepo;
global $cache;

$key = sprintf('appPage#%s#%s', $_GET['type'], $_GET['url']);
$pageContent = $cache->get($key);
$ttl = 300;

if ($pageContent === false) {
    ob_start();

    get_header();
    $paged = 1;
    switch ($_GET['type']) {
        case 'page':
            if ($_GET['url'] === 'blic-naslovna') {
                handleEuroBlic();
                $ttl = 2400;
                break;
            }
            handleCategory($paged);
            $ttl = 60;
            break;

        case 'author':
            handleAuthor($paged);
            $ttl = 300;
            break;

        case 'tag':
            handleTag($paged);
            $ttl = 600;
            break;

        case 'static':
            handleStatic();
            $ttl = 2400;
            break;

        case 'article':
            handleArticle();
            $ttl = 600;
            break;
        case 'search':
            handleSearch();
            $ttl = false;
            break;
    }

    $pageContent = ob_get_clean();
    if (!is_user_logged_in() && $ttl) {
        $cache->set($key, $pageContent, $ttl);
    }
}
echo $pageContent;

function handleSearch() {
    global $dotMetricsId;
    $dotMetricsId = getDotMetricsId('other');
    $searchQuery = $_GET['url'];
    $sortedItems = ArticleRepo::elasticSearch($searchQuery);
    //Needed for infinite scroll
    $ajaxAction = 'search';
    $ajaxTermValue = $searchQuery;
    include "templates/archive/archiveMobileApp.php";
    wp_footer();
}

function handleEuroBlic() {
    global $dotMetricsId;
    $id = get_option('blicNaslovnaImg');
    $imgSrc = wp_get_attachment_image_src($id,'euro-blic-m')[0];
    $imgSrcLarge = wp_get_attachment_image_src($id,'euro-blic-d')[0];
    $dotMetricsId = getDotMetricsId('other');
    ?>
    <div class="container">
        <div class="siCategory">
            <div class="category__left">
                <div>
                    <img id="myImg" class="blicNaslovnaPage" src="<?=$imgSrc?>" data-image-large="<?=$imgSrcLarge?>" title="blic naslovna" alt="blic naslovna">
                    <div id="myModal" class="modal">
                        <span class="close">&times;</span>
                        <img class="modal-content" id="img01">
                    </div>
                </div>
            </div>
            <aside class="category__right"> <!-- Category Right Side -->
                <?php dynamic_sidebar( 'blic_naslovna_sidebar' ); ?>
            </aside><!-- Category Right Side End -->
        </div>
    </div>
    <script type="text/javascript" src="<?= PARENT_THEME_DIR_URI . '/js/blicNaslovnaPopUp/blicNaslovnaPopUp.js'?>"></script>

    <?php
    get_footer();
}

function handleArticle() {
    //Dotmetrics id is in template
    dynamic_sidebar('single_post_top_banner');

    include 'templates/single/singleMobileApp.php';
    get_footer();
}

function handleStatic() {
    global $dotMetricsId;
    $dotMetricsId = getDotMetricsId('other');
    $page = get_page_by_path($_GET['url']);
    if($_GET['url'] === 'pravila-koriscenja') {
        echo '<div class="staticWrapper">';
    } elseif($_GET['url'] === 'impressum' || $_GET['url'] === 'marketing') {
        echo '<div class="staticWrapperImpressum">';
    }
    ?>
        <div id="post-<?php $page->ID; ?>" <?php post_class( 'post-wrapper' ); ?>>
        <?php echo $page->post_content; ?>
        </div>
    </div>

    <?php
    get_footer();
}

function handleTag($paged) {
    global $dotMetricsId;
    $dotMetricsId = getDotMetricsId('other');
    $tag = get_term_by('slug', $_GET['url'], 'post_tag');
    $catName = $tag->name;
//    Needed for infinite scroll
    $ajaxAction = 'tag';
    $ajaxTermValue = $_GET['url'];
    $sortedItems = ArticleRepo::getItemsFromElasticBy($ajaxAction, $tag, $paged, PER_PAGE);

    include "templates/archive/archiveMobileApp.php";
    wp_footer();
}

function handleAuthor($paged) {
    global $dotMetricsId;
    $dotMetricsId = getDotMetricsId('other');
    $author = get_user_by('slug', $_GET['url']);
    $catUrl = parseAppUrl('author', $_GET['url']);
    $catName = $author->display_name;
//    Needed for infinite scroll
    $ajaxAction = 'author';
    $ajaxTermValue = $_GET['url'];
    $sortedItems = ArticleRepo::getItemsFromElasticBy($ajaxAction, $author, $paged, PER_PAGE);

    include "templates/archive/archiveMobileApp.php";
    wp_footer();
}

function handleCategory($paged) {
    global $dotMetricsId;

    if ($_GET['url'] === 'sve-vijesti') {
        $catName = 'Sve vijesti';
        //Sve vijesti stranica za app ima odvojen dotmetricId od desktopa zato je slug drugaciji od url-a stranice
        $dotMetricsId = getDotMetricsId('novo');
        $args = [
            'paged' => $paged,
            'posts_per_page' => PER_PAGE,
            'orderby' => 'DESC'
        ];
        $cat = new \Stdclass();
        $cat->term_id = 0;
        $ajaxTermValue = 'sve-vijesti';
        $type = 'sve-vijesti';
    } else if ($_GET['url'] === 'popularno') {
        $dotMetricsId = getDotMetricsId('popularno');
        $args = [
            'paged' => $paged,
            'numberposts' => PER_PAGE,
            'order' => 'DESC',
            'meta_key' => 'gfPostViewCount',
            'orderby' => 'meta_value_num'
        ];
        //For now because popular posts page is not complete popular links go to sve-vijesti
        $cat = new \Stdclass();
        $cat->term_id = 0;
        $catName = 'Popularno';
        $type = 'sve-vijesti';
        $ajaxTermValue = 'sve-vijesti';
    } else {
        $cat = get_term_by('slug', $_GET['url'], 'category');
        $catName = $cat->name;
        $ajaxTermValue = $_GET['url'];
        $dotMetricsId = getDotMetricsId($cat->slug);
        $type = 'category';
//            $catUrl = '/' . $cat->slug . '/';
        $args = [
            'category_name' => $catName,
            'paged' => $paged,
            'posts_per_page' => PER_PAGE
        ];
    }
    //Needed for infinite scroll
    $ajaxAction = 'category';
    if (USE_ELASTIC !== true) {
        $sortedItems = ArticleRepo::getItemsFromWp($args);
    } else {
        $sortedItems = ArticleRepo::getItemsFromElasticBy($type, $cat, $paged, PER_PAGE);
    }

    include "templates/archive/archiveMobileApp.php";
    wp_footer();
}