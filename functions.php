<?php
ini_set('display_errors', DISPLAY_ERRORS);

use Carbon\Carbon;
use GfWpPluginContainer\Wp\WpEnqueue;
use \GfWpPluginContainer\Indexer\Repository\Article as ArticleRepo;
use \GfWpPluginContainer\Indexer\Repository\Image as ImageRepo;


if (@$_GET['_fields'] === 'id,name' && @$_GET['_locale'] === 'user' && @$_GET['per_page'] > 20) {
    // malformed request, redirect to home to avoid spending cpu cycles
    if (strpos($_SERVER['REQUEST_URI'], 'index.php')) {
        wp_safe_redirect(home_url());
        exit();
    }
}


define('CHILD_THEME_DIR', get_stylesheet_directory());
define('CHILD_THEME_DIR_URI', get_stylesheet_directory_uri());
define('PARENT_THEME_DIR', get_template_directory());
define('PARENT_THEME_DIR_URI', get_template_directory_uri());
define('CONTAINER_PLUGIN_NAME', 'srpskapluginscontainer/GfShopThemePlugins.php');
$homePage = get_page_by_title('Početna');
$homePageID = $homePage->ID;

$themeConfig = require_once(__DIR__ . '/config/themeConfig.php');
include_once(ABSPATH . 'wp-admin/includes/plugin.php');
if (is_admin()) {
    include_once(ABSPATH . 'wp-admin/includes/plugin.php');
    if (!is_plugin_active(CONTAINER_PLUGIN_NAME)) {
        add_action('admin_notices', ['GfWpPluginContainer\Wp\WpHelper', 'noPluginNotice']);
//        exit();
    }
}
$theme = new \GfWpPluginContainer\Wp\Setup($themeConfig);
$dotMetricsId = '';

$isApp = false !== strpos($_SERVER['REQUEST_URI'], 'mobile-home/') || strpos(
        $_SERVER['REQUEST_URI'],
        '/app/'
    ) !== false;

function isAmp()
{
    return function_exists('is_amp_endpoint') && is_amp_endpoint();
}

if (class_exists('Redis')) {
    $engine = new \Redis();
    $engine->connect('127.0.0.1');
} else {
    $engine = new \Memcached();
    $engine->addServer('127.0.0.1', 11211);
}
$cache = new \GfWpPluginContainer\Wp\Cache($engine);

/* container for displayed posts on a single page */
$restrictedPosts = [];
if (defined('WP_CLI') && WP_CLI) {
    $cli = new GfWpPluginContainer\Wp\Cli();
}

add_image_size('medium', 120, 82, true);
add_image_size('large', 374, 250, true);
add_image_size('mobile-m', 304, 204, true); // 335x224
add_image_size('list-big', 427, 285, true);
add_image_size('list-small', 275, 188, true);
add_image_size('single', 872, 610, true);
add_image_size('euro-blic', 406, 271, true);
add_image_size('related', 208, 142, true);
add_image_size('euro-blic-d', 790, 1036, true);
add_image_size('euro-blic-m', 330, 433, true);
add_image_size('portrait', 800, 1200, false);
add_image_size('portrait-m', 400, 800, false);
add_image_size('landsape-m', 400, 200, false);


//$post = get_post(135)->to_array();

//for ($i = 0; $i < 10; $i++) {
//    $ip = [
//        'post_author' => $post['post_author'],
//        'post_content' => $post['post_content'],
//        'post_title' => $post['post_title'],
//        'post_status' => $post['post_status'],
//        'post_type' => $post['post_type'],
//        'post_category' => $post['post_category']
//    ];
//    $id = wp_insert_post($ip);
//    set_post_thumbnail($id, get_post_thumbnail_id($post['ID']));
//    wp_set_post_tags($id,['krađa', 'izbori']);
//}

//remove_filter( 'the_content', 'wpautop' );

// Add IDs for posts in the admin panel
add_filter('manage_posts_columns', 'addIdColumn', 5);
add_action('manage_posts_custom_column', 'idColumnContent', 5, 2);

/* Take the first two items in the columns array (checkbox and title), and take the rest of the items.
    Merge these 2 arrays, with our custom ID column in the middle */
function addIdColumn($columns)
{
    $checkboxAndTitle = array_slice($columns, 0, 2);
    $restOfTheColumns = array_slice($columns, 2);
    $id['id'] = 'ID';


    $columns = array_merge($checkboxAndTitle, $id, $restOfTheColumns);

    return $columns;
}

/* Check if the column is our custom ID column
    and echo the id of the post inside(post id is passed to the function so we just echo it) */
function idColumnContent($column, $id)
{
    if ($column === 'id') {
        echo $id;
    }
}


//$isApp = $_SERVER['REQUEST_URI'] === '/mobile-home/' || strpos( $_SERVER['REQUEST_URI'], '?isapp=true' ) !== false;
global $isApp;
if ($isApp) {
    WpEnqueue::addFrontendScript('ptr', PARENT_THEME_DIR_URI . '/js/app/ptr.js');
    WpEnqueue::addFrontendScript('is', PARENT_THEME_DIR_URI . '/js/app/infiniteScroll.js');
}

// Change the default number of characters that excerpts print out
add_filter('excerpt_length', function ($length) {
    return 22;
});

/**
 * Extracts heading or first p from block.
 *
 * @param $blocks
 * @return mixed|string
 */
function parseHeading($blocks)
{
    $heading = '';
    // If there is a heading block, get the first one
    if (isset($blocks) && is_array($blocks)) {
        if (isset($blocks[0]) && $blocks[0]['blockName'] === 'core/heading') {
            $heading = $blocks[0]['innerHTML'];
            $heading = str_replace(['<h2>', '</h2>'], '', $heading);
            return $heading;
        }
        if (isset($blocks[0]) && $blocks[0]['blockName'] === 'core/paragraph') {
            return $blocks[0]['innerHTML'];
        }
    }

    //if heading does not exist return first paragraph in the post body
    if ($heading === '') {
        //In case the first element is not a paragraph get the first paragraph
        if (!is_array($blocks)) {
            return $heading;
        }
        foreach ($blocks as $block) {
            if ($block['blockName'] === 'core/paragraph') {
                $heading = $block['innerHTML'];
                break;
            }
            if (!wp_is_mobile()) {
                $block = apply_filters('the_content', render_block($block));
                $start = mb_strpos($block, '<p>');
                $heading = mb_substr($block, $start, mb_strpos($block, '</p>', $start) - $start + 4);
                break;
            }
        }
    }

    return $heading;
}

function parseAppUrl($type, $url)
{
    if (false !== strpos($url, 'pocetna')) {
        return home_url('/mobile-home/');
    }

    if (in_array(str_replace([home_url(), '/'], '', $url), ['impressum', 'marketing', 'pravila-koriscenja'])) {
        return sprintf('/app/?url=%s&type=static', str_replace([home_url(), '/'], '', $url));
    }

    switch ($type) {
        case 'page':
            $url = sprintf('/app/?url=%s&type=page', str_replace([home_url(), '/'], '', $url));

            break;

        case 'article':
            $url = sprintf('/app/?url=%s&type=article', str_replace([home_url(), '/'], '', $url));

            break;

        case 'author':
            $url = sprintf('/app/?url=%s&type=author', str_replace([home_url('/author/'), '/'], '', $url));

            break;

        case 'tag':
            $url = sprintf('/app/?url=%s&type=tag', str_replace([home_url('/tag/'), '/'], '', $url));

            break;
    }

    return $url;
}

//Infinite Scroll
function infiniteScroll()
{
    $paged = $_POST['page'];
    $action = $_POST['gfAction'];
    $value = $_POST['termValue'];
    $perPage = PER_PAGE;
    $useElastic = true;
    switch ($action) {
        case 'author':
            $author = $value;
            $catUrl = '/author/' . $author . '/';
            $catUrl = parseAppUrl('author', $catUrl);
            $author = get_user_by('login', $author);
            if (!$useElastic) {
                $sortedItems = ArticleRepo::getItemsFromWp(
                    [
                        'author' => $value,
                        'paged' => $paged,
                        'posts_per_page' => PER_PAGE
                    ]
                );
            } else {
                $sortedItems = ArticleRepo::getItemsFromElasticBy($action, $author, $paged, $perPage);
            }
            break;
        case 'category':
            $catName = $value;
            switch ($value) {
                case 'sve-vijesti':
                    $args = [
                        'paged' => $paged,
                        'posts_per_page' => PER_PAGE,
                        'orderby' => 'DESC'
                    ];
                    $catObject = new \Stdclass();
                    $catObject->term_id = 0;
                    $action = $value;
                    break;

                case 'popularno':
                    $args = [
                        'paged' => $paged,
                        'numberposts' => PER_PAGE,
                        'order' => 'DESC',
                        'meta_key' => 'gfPostViewCount',
                        'orderby' => 'meta_value_num'
                    ];
                    //@todo implement elastic for popular posts
                    $catObject = new \Stdclass();
                    $catObject->term_id = 0;
//                    $action = $value;
                    $action = 'sve-vijesti';
                    break;

                default:
                    $catObject = get_term_by('slug', $value, 'category');
                    $args = [
                        'category_name' => $catName,
                        'paged' => $paged,
                        'posts_per_page' => PER_PAGE
                    ];
                    break;
            }
            if (!$useElastic) {
                $sortedItems = ArticleRepo::getItemsFromWp($args);
            } else {
                $sortedItems = ArticleRepo::getItemsFromElasticBy($action, $catObject, $paged, PER_PAGE);
            }

            break;

        case 'search':
            $searchQuery = $value;
            if (!$useElastic) {
                //@todo implementirati ponasanje ako nema elastica
            } else {
                $sortedItems = ArticleRepo::elasticSearch($searchQuery, $paged);
            }

            break;
        case 'tag':
            $searchQuery = $value;
            $tag = get_term_by('slug', $value, 'post_tag');
            if (!$useElastic) {
                //@todo implementirati ponasanje ako nema elastica
            } else {
                $sortedItems = ArticleRepo::getItemsFromElasticBy('tag', $tag, $paged, $perPage);
            }

            break;
    }
    if (count($sortedItems['articles'])) {
        /* @var \GfWpPluginContainer\Wp\Article $article */
        foreach ($sortedItems['articles'] as $key => $article) {
            $blocks = null;
            $permalink = parseAppUrl('article', $article->getPermalink());
            $catUrl = parseAppUrl('page', $article->getCategory()['slug']);
            $thumbnailSrc = $article->getThumbnail();
            $title = $article->getTitle();
            $catName = mb_strtolower($article->getCategory()['name']);

            $postId = $article->getPostId();
            $authorId = get_post_field('post_author', $postId);
            $authorName = get_the_author_meta('display_name', $authorId);
            $authorUrl = parseAppUrl('author', get_author_posts_url($authorId));

            Carbon::setLocale('bs');
            $postDate = new Carbon($article->getPublishedAt(), new \DateTimeZone('Europe/Sarajevo'));
            $timeAgo = explode(' ', $postDate->longAbsoluteDiffForHumans());
            $imageUrl = get_post_thumbnail_id($article->getPostId()) ?: get_option("defaultFeaturedImage");
            if ($key < 1) {
                $post['imageUrl'] = esc_url(wp_get_attachment_image_url($imageUrl, 'large'));
                $post['imageWidth'] = 374;
                $post['imageHeight'] = 250;
            } else {
                $post['imageUrl'] = esc_url(wp_get_attachment_image_url($imageUrl, 'medium'));
                $post['imageWidth'] = 120;
                $post['imageHeight'] = 82;
            }
            $infiniteScroll = true;
            include(__DIR__ . '/templates/loop/categoryArticle.php');
        }
    }
    exit;
}

add_action('wp_ajax_infinite_scroll', 'infiniteScroll'); // for logged in user
add_action('wp_ajax_nopriv_infinite_scroll', 'infiniteScroll'); // if user not logged in

//Ajax for blic naslovna update
function updateBlicNaslovna()
{
    $blicNaslovnaSrc = $_POST['blicNaslovnaImageUrl'];
    update_option('blicNaslovnaImg', $blicNaslovnaSrc);
}

add_action('wp_ajax_updateBlicNaslovna', 'updateBlicNaslovna');
add_action('wp_ajax_nopriv_updateBlicNaslovna', 'updateBlicNaslovna');

//Checks if role is present in current user roles
function checkForLoggedInUserRole($needle): bool
{
    $currentUser = wp_get_current_user();
    $currentUserRole = $currentUser->roles;
    foreach ($currentUserRole as $role) {
        if ($role === $needle) {
            return true;
        }
    }
    return false;
}

if (checkForLoggedInUserRole('editor') || checkForLoggedInUserRole('administrator')) {
    add_action(
        'admin_menu',
        function () {
            add_menu_page(
                'Izmeni naslovnu',
                'Izmeni naslovnu',
                'edit_others_pages',
                'edit-home',
                '',
                'dashicons-text',
                11
            );
        }
    );
    add_action('admin_menu', 'editHomeHardLink');
}

function editHomeHardLink()
{
    // Get the ID of the page that is set as the static homepage page
    global $menu;
    $menu[11][2] = home_url('/wp-admin/post.php?post=' . get_option('page_on_front') . '&action=edit');
}

if (isset($_GET['post'])) {
    add_action('current_screen', 'thisScreen');
}
// Limiting block usage for certain roles
function thisScreen()
{
    // check if the current page is being edited, and if the page name is $pageName
    $currentScreen = get_current_screen();
    $pageName = get_the_title($_GET['post']);
    if ($currentScreen->id === 'page' && $pageName === 'Početna') {
        add_filter('allowed_block_types', 'allowedBlockTypesInEdit');
    }
}

function allowedBlockTypesInEdit()
{
    $currentUser = wp_get_current_user();
    $currentUserRole = $currentUser->roles;
    // hide banner management from non admins
    foreach ($currentUserRole as $role) {
        if (checkForLoggedInUserRole('editor') || checkForLoggedInUserRole('author')) {
            echo '<style>
.gfBannerBlockEditor {display:none}
</style>';
        }

        /*  If the user role is author, only allow the use of certain blocks in the edit section of $pageName.
            The allowed blocks are listed in the return part of this function	*/
        if (checkForLoggedInUserRole('author')) {
            return [
                'wpplugincontainer/gfcategoriesblock',
                'wpplugincontainer/gfnewsblock',
//            'wpplugincontainer/gfbannerblock',
                'wpplugincontainer/gfblicblock',
                'wpplugincontainer/gfhpgblock',
                'wpplugincontainer/gfnewssidebar',
                'wpplugincontainer/gfcategorysidebar',
                'wpplugincontainer/gfcategoryshowcase',
                'core/shortcode',
            ];
        }
    }
}

// Get the current user, if its author or editor remove the tools section from the dashboard

add_action(
    'admin_menu',
    function () {
        if (checkForLoggedInUserRole('author') || checkForLoggedInUserRole('editor')) {
            remove_menu_page('tools.php'); // Tools
            remove_menu_page('edit.php?post_type=page'); //Pages
            remove_menu_page('edit-comments.php'); //Comments
            remove_menu_page('options-general.php'); // Settings
            remove_menu_page('themes.php'); // Appearance
            remove_menu_page('rank-math'); // Rank math
            remove_menu_page('amp-options'); // Amp options
            remove_menu_page('w3tc_dashboard'); // Performance
            remove_menu_page('smush'); // Smush Pro
            remove_menu_page('wp-tweets-pro'); // Wp to twitter
            remove_menu_page('to-top'); // To top plugin
            remove_menu_page('wp-mail-smtp'); //Wp mail smtp
            remove_menu_page('default-featured-image'); // GF featured image
            add_action('admin_head', 'removeTopMenuSettings');
            function removeTopMenuSettings()
            {
                echo '<style>
                    #wp-admin-bar-clearfy-menu,
                    #wp-admin-bar-w3tc,
                    #wp-admin-bar-query-monitor 
                    {display:none !important;}

                   </style>';
            }
        }
    }
);

add_action(
    'admin_menu',
    function () {
        if (checkForLoggedInUserRole('author')) {
            remove_menu_page('blic-naslovna');
        }
    }
);


// Hide some sidebars from the admin for editors (pass the id of the sidebar)
$currentUser = wp_get_current_user();
$currentUserRole = $currentUser->roles;
if (checkForLoggedInUserRole('editor')) {
    global $pagenow;

    if ($pagenow === 'widgets.php') {
        echo "<style>
                #category_page_top_banner, #single_post_top_banner,#category_mobile_feed_baner_1, #category_mobile_feed_baner_2,
                 #category_mobile_feed_baner_3, #category_mobile_feed_baner_4, #ampforwp-above-loop, #ampforwp-below-loop,
                  #ampforwp-below-header, #ampforwp-above-footer {
                    display:none;
                }
            </style>";
    }
}
if (checkForLoggedInUserRole('editor') || checkForLoggedInUserRole('author')) {
    global $pagenow;

    if ($pagenow === 'post.php') {
        echo "<style>
                .components-notice__content, .components-notice, .is-notice {
                    display:none !important;
                }
            </style>";
    }
}


// Get page id by page name and make that the only available page for authors and editors
add_filter('parse_query', 'excludePagesFromAdminAuthor');
function excludePagesFromAdminAuthor($query)
{
    global $homePageID;
    global $pagenow, $post_type;
    if (checkForLoggedInUserRole('author') || checkForLoggedInUserRole('editor')) {
        if (is_admin() && $pagenow == 'edit.php' && $post_type == 'page') {
            $query->query_vars['post__in'] = [$homePageID];
        }
    }
}

// Get page ids by template and hide them for editors in the page list
//add_filter( 'parse_query', 'excludePagesFromAdminEditor' );
//function excludePagesFromAdminEditor($query) {
//    $pages = get_pages([
//        'post_type' => 'page',
//        'meta_key' => '_wp_page_template',
//        'hierarchical' => 0,
//        'meta_value' => 'customCategory.php'
//    ]);
//    $pageIDs = [];
//    foreach($pages as $page) {
//        $pageIDs[] = $page->ID;
//    }
//    $currentUser = wp_get_current_user();
//    $currentUserRole = $currentUser->roles;
//    global $pagenow,$post_type;
//    if ((count($currentUserRole) === 1) && $currentUserRole[0] === 'editor') {
//        if (is_admin() && $pagenow == 'edit.php' && $post_type == 'page') {
//            $query->query_vars['post__not_in'] = $pageIDs;
//        }
//    }
//}


/* scripts: tracking, ads */
include(__DIR__ . '/banners.php');

//$dotMetricsId = 4595;
function getDotMetricsId($slug)
{
    switch (strtolower($slug)) {
        case 'naslovna':  // hardcoded in footer
            return 3934;
            break;
        case 'sve-vijesti':
            return 3935;
            break;
        case 'politika':
            return 3936;
            break;
        case 'svijet':
            return 3937;
            break;
        case 'drustvo':
            return 3938;
            break;
        case 'biznis':
            return 3939;
            break;
        case 'hronika':
            return 3940;
            break;
        case 'sport':
            return 3941;
            break;
        case 'zabava':
            return 3942;
            break;
        case 'kultura':
            return 3943;
            break;
        case 'kolumne':
            return 3944;
            break;
        case 'banjaluka':
            return 3945;
            break;
        case 'regioni':
        case 'gradovi':
            return 3946;
            break;
        case 'novo':
            return 3956;
            break;
        case 'popularno':
            return 3957;
            break;
        case 'digitalna-srpska':
            return 4247;
            break;
        case 'sudbine':
            return 11334;
            break;
        case 'other':
        default:
            return 4595;
    }
}

add_action('delete_post', 'deletePost', 10);
function deletePost($post_ID)
{
    global $wpdb;
    $post = get_post($post_ID);
//    $searchFunctions = new \GfWpPluginContainer\Elastic\Functions($wpdb, true);
//    $searchFunctions->deleteItemFromElastic($post);
}

add_action('wp_insert_post', 'syncPost', 10, 3);
function syncPost($post_ID, $post, $update)
{
    ArticleRepo::syncItemToElastic($post);
}

add_action('add_attachment', 'syncImageOnCreate', 666, 5);
function syncImageOnCreate($post_id)
{
    global $wpdb;
    $item = get_post($post_id);
    ImageRepo::syncImageToElastic($item);
}

add_action('edit_attachment', 'syncImageOnUpdate', 10, 5);
function syncImageOnUpdate($post_id)
{
    global $wpdb;
    $post = get_post($post_id);
    ImageRepo::syncImageToElastic($post);
}

add_action('admin_head', 'printUserRole');
function printUserRole()
{
    $role = 'editor';
    if (checkForLoggedInUserRole('author')) {
        $role = 'author';
    }
    echo '<script type="text/javascript">let userRole = "'.$role.'";</script>';
}

add_filter('heartbeat_settings', 'setHeartBeatRate');
function setHeartBeatRate($settings)
{
    $settings['interval'] = 60; //Anything between 15-60
    return $settings;
}


// AMP
add_action('amp_post_template_css', 'cssForAmp', 9999);
function cssForAmp()
{
    $path = PARENT_THEME_DIR_URI . '/';
    echo include PARENT_THEME_DIR . '/amp/css/style.css';
}

add_action('wp_head', 'addGoogleAnalytics');
function addGoogleAnalytics()
{
    global $isApp;

    if ($isApp) { ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121882007-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());
            gtag('config', 'UA-121882007-1');
        </script>
        <?php
        $ua = strtolower($_SERVER['HTTP_USER_AGENT'] . '');
        if (false !== strpos($ua, 'iosiphone') || false !== strpos($ua, 'iosipad')): ?>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=G-ZPDJM2KH85"></script>
            <script>
                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('js', new Date());
                gtag('config', 'G-ZPDJM2KH85');
            </script>
        <?php
        else: ?>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=G-LSFDCG8NEG"></script>
            <script>
                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('js', new Date());
                gtag('config', 'G-LSFDCG8NEG');
            </script>
        <?php
        endif; ?><?php
    } else { ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-83076923-7"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-83076923-7');
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-B2LH3N11T9"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-B2LH3N11T9');
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-166909478-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-166909478-1');
        </script>
        <?php
    } ?><?php
}

add_action('wp_footer', 'addDotMetrics', 1);
function addDotMetrics()
{ ?><?php
    global $dotMetricsId;
    if (is_front_page()) {
        $dotMetricsId = 3934;
    } ?>
    <script type="text/javascript">
        /* <![CDATA[ */
        (function () {
            window.dm = window.dm || {AjaxData: []};
            window.dm.AjaxEvent = function (et, d, ssid, ad) {
                dm.AjaxData.push({et: et, d: d, ssid: ssid, ad: ad});
                window.DotMetricsObj && DotMetricsObj.onAjaxDataUpdate();
            };
            var d = document,
                h = d.getElementsByTagName('head')[0],
                s = d.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = document.location.protocol + '//script.dotmetrics.net/door.js?id=<?=$dotMetricsId?>';
            h.appendChild(s);
        }());
        /* ]]> */
    </script>
    <?php
}

//Media uploader fields
add_filter('attachment_fields_to_edit', 'addCustomFieldsToMedia', 1, 2);
/**
 * Adds custom field to media upload fields
 * @param $fields , An array of attachment form fields.
 * @param $post ,  WP_Post attachment object.
 *
 * @return array
 */
function addCustomFieldsToMedia($fields, $post): array
{
    $imageUploaderId = get_post_field('post_author', $post->ID);
    $imageUploaderName = get_the_author_meta('display_name', $imageUploaderId);
    $fields['legenda'] = [
        'label' => 'Legenda',
        'input' => 'text',
        'value' => get_post_meta($post->ID, 'legenda', true),
        'helps' => '',
    ];
    $fields['Uploaded by'] = [
        'label' => 'Postavio/-la',
        'input' => 'html',
        'html' => '<input type="text" readonly="readonly" value="' . esc_attr($imageUploaderName) . '"/>',
        'value' => $imageUploaderName
    ];
    return $fields;
}

add_filter('attachment_fields_to_save', 'saveCustomFieldsToMedia', 10, 2);
/**
 * Save values of legendaField in media uploader
 *
 * @param $post array, the post data for database
 * @param $attachment array, attachment fields from $_POST form
 * @return $post array, modified post data
 */

function saveCustomFieldsToMedia($post, $attachment)
{
    if (isset($attachment['legenda'])) {
        update_post_meta($post['ID'], 'legenda', $attachment['legenda']);
    }
    return $post;
}

/**
 * Override the "Attachments Details" Backbone micro template in WordPress 4.0
 *
 * @see https://stackoverflow.com/a/25948448/2078474
 */

add_action('admin_footer-post.php', 'modifyMediaUploadFields');
add_action('admin_footer-post-new.php', 'modifyMediaUploadFields');

function modifyMediaUploadFields()
{
    ?>
    <script type="text/html" id="tmpl-attachment-details-custom">
        <h3>
            <?php
            _e('Attachment Details'); ?>
            <span class="settings-save-status">
                                <span class="spinner"></span>
                                <span class="saved"><?php
                                    esc_html_e('Saved.'); ?></span>
                        </span>
        </h3>
        <div class="attachment-info">
            <div class="thumbnail thumbnail-{{ data.type }}">
                <# if ( data.uploading ) { #>
                <div class="media-progress-bar">
                    <div></div>
                </div>
                <# } else if ( 'image' === data.type && data.sizes ) { #>
                <img src="{{ data.size.url }}" draggable="false"/>
                <# } else { #>
                <img src="{{ data.icon }}" class="icon" draggable="false"/>
                <# } #>
            </div>
            <div class="details">
                <div class="filename">{{ data.filename }}</div>
                <div class="uploaded">{{ data.dateFormatted }}</div>
                <div class="file-size">{{ data.filesizeHumanReadable }}</div>
                <# if ( 'image' === data.type && ! data.uploading ) { #>
                <# if ( data.width && data.height ) { #>
                <div class="dimensions">{{ data.width }} &times; {{ data.height }}</div>
                <# } #>
                <# if ( data.can.save && data.sizes ) { #>
                <a class="edit-attachment" href="{{ data.editLink }}&amp;image-editor" target="_blank"><?php
                    _e('Edit Image'); ?></a>
                <a class="refresh-attachment" href="#"><?php
                    _e('Refresh'); ?></a>
                <# } #>
                <# } #>
                <# if ( data.fileLength ) { #>
                <div class="file-length"><?php
                    _e('Length:'); ?> {{ data.fileLength }}
                </div>
                <# } #>
                <# if ( ! data.uploading && data.can.remove ) { #>
                <?php
                if (MEDIA_TRASH): ?>
                    <# if ( 'trash' === data.status ) { #><a class="untrash-attachment" href="#"><?php
                        _e('Untrash'); ?></a><# } else { #><a class="trash-attachment" href="#"><?php
                        _e('Trash'); ?></a><# } #>
                <?php
                else: ?>
                    <a class="delete-attachment" href="#"><?php
                        _e('Delete Permanently'); ?></a>
                <?php
                endif; ?>
                <# } #>
                <div class="compat-meta">
                    <# if ( data.compat && data.compat.meta ) { #>
                    {{{ data.compat.meta }}}
                    <# } #>
                </div>
            </div>
        </div><label class="setting" data-setting="url">
            <span class="name"><?php
                _e('URL'); ?></span>
            <input type="text" value="{{ data.url }}" readonly/>
        </label><# var maybeReadOnly = data.can.save || data.allowLocalEdits ? '' : 'readonly'; #><!--        <label class="setting" data-setting="title">--><!--            <span class="name">--><?php
        //_e('Title');
        ?><!--</span>--><!--            <input type="text" value="{{ data.title }}" {{ maybeReadOnly }} />--><!--        </label>--><# if ( 'audio' === data.type ) { #>
        <?php
        foreach (
            [
                'artist' => __('Artist'),
                'album' => __('Album'),
            ] as $key => $label
        ) : ?>
            <label class="setting" data-setting="<?php
            echo esc_attr($key) ?>">
                <span class="name"><?php
                    echo $label ?></span>
                <input type="text" value="{{ data.<?php
                echo $key ?> || data.meta.<?php
                echo $key ?> || '' }}"/>
            </label>
        <?php
        endforeach; ?>
        <# } #><# if ( 'image' === data.type ) { #><label class="setting" data-setting="caption">
            <span class="name"><?php
                _e('Autor'); ?></span>
            <textarea {{ maybeReadOnly }}>{{ data.caption }}</textarea>
        </label><# } #><!-- LET'S REMOVE THIS SECTION:
                <# if ( 'image' === data.type ) { #>
                        <label class="setting" data-setting="alt">
                                <span class="name"><?php
        _e('Alt Text'); ?></span>
                                <input type="text" value="{{ data.alt }}" {{ maybeReadOnly }} />
                        </label>
                <# } #>
                <label class="setting" data-setting="description">
                        <span class="name"><?php
        _e('Description'); ?></span>
                        <textarea {{ maybeReadOnly }}>{{ data.description }}</textarea>
                </label>
-->
    </script>
    <script>
        jQuery(document).ready(function ($) {
            if (typeof wp.media.view.Attachment.Details != 'undefined') {
                wp.media.view.Attachment.Details.prototype.template = wp.media.template('attachment-details-custom');
            }
        });
    </script>
    <?php
}

/**
 * Override the "Attachments Details Two Column" Backbone micro template in WordPress 4.0
 *
 * @see https://stackoverflow.com/a/25948448/2078474
 */

add_action('admin_footer-upload.php', 'modifyMediaUploadFieldsMediaUploadSection');

function modifyMediaUploadFieldsMediaUploadSection()
{ ?>
    <script type="text/html" id="tmpl-attachment-details-two-column-custom">
        <div class="attachment-media-view {{ data.orientation }}">
            <div class="thumbnail thumbnail-{{ data.type }}">
                <# if ( data.uploading ) { #>
                <div class="media-progress-bar">
                    <div></div>
                </div>
                <# } else if ( 'image' === data.type && data.sizes && data.sizes.large ) { #>
                <img class="details-image" src="{{ data.sizes.large.url }}" draggable="false"/>
                <# } else if ( 'image' === data.type && data.sizes && data.sizes.full ) { #>
                <img class="details-image" src="{{ data.sizes.full.url }}" draggable="false"/>
                <# } else if ( -1 === jQuery.inArray( data.type, [ 'audio', 'video' ] ) ) { #>
                <img class="details-image" src="{{ data.icon }}" class="icon" draggable="false"/>
                <# } #>
                <# if ( 'audio' === data.type ) { #>
                <div class="wp-media-wrapper">
                    <audio style="visibility: hidden" controls class="wp-audio-shortcode" width="100%" preload="none">
                        <source type="{{ data.mime }}" src="{{ data.url }}"/>
                    </audio>
                </div>
                <# } else if ( 'video' === data.type ) {
                var w_rule = h_rule = '';
                if ( data.width ) {
                w_rule = 'width: ' + data.width + 'px;';
                } else if ( wp.media.view.settings.contentWidth ) {
                w_rule = 'width: ' + wp.media.view.settings.contentWidth + 'px;';
                }
                if ( data.height ) {
                h_rule = 'height: ' + data.height + 'px;';
                }
                #>
                <div style="{{ w_rule }}{{ h_rule }}" class="wp-media-wrapper wp-video">
                    <video controls="controls" class="wp-video-shortcode" preload="metadata"
                    <# if ( data.width ) { #>width="{{ data.width }}"<# } #>
                    <# if ( data.height ) { #>height="{{ data.height }}"<# } #>
                    <# if ( data.image && data.image.src !== data.icon ) { #>poster="{{ data.image.src }}"<# } #>>
                    <source type="{{ data.mime }}" src="{{ data.url }}"/>
                    </video>
                </div>
                <# } #>
                <div class="attachment-actions">
                    <# if ( 'image' === data.type && ! data.uploading && data.sizes && data.can.save ) { #>
                    <a class="button edit-attachment" href="#"><?php
                        _e('Edit Image'); ?></a>
                    <# } #>
                </div>
            </div>
        </div>
        <div class="attachment-info">
                        <span class="settings-save-status">
                                <span class="spinner"></span>
                                <span class="saved"><?php
                                    esc_html_e('Saved.'); ?></span>
                        </span>
            <div class="details">
                <div class="filename"><strong><?php
                        _e('File name:'); ?></strong> {{ data.filename }}
                </div>
                <div class="filename"><strong><?php
                        _e('File type:'); ?></strong> {{ data.mime }}
                </div>
                <div class="uploaded"><strong><?php
                        _e('Uploaded on:'); ?></strong> {{ data.dateFormatted }}
                </div>
                <div class="file-size"><strong><?php
                        _e('File size:'); ?></strong> {{ data.filesizeHumanReadable }}
                </div>
                <# if ( 'image' === data.type && ! data.uploading ) { #>
                <# if ( data.width && data.height ) { #>
                <div class="dimensions"><strong><?php
                        _e('Dimensions:'); ?></strong> {{ data.width }} &times; {{ data.height }}
                </div>
                <# } #>
                <# } #>
                <# if ( data.fileLength ) { #>
                <div class="file-length"><strong><?php
                        _e('Length:'); ?></strong> {{ data.fileLength }}
                </div>
                <# } #>
                <# if ( 'audio' === data.type && data.meta.bitrate ) { #>
                <div class="bitrate">
                    <strong><?php
                        _e('Bitrate:'); ?></strong> {{ Math.round( data.meta.bitrate / 1000 ) }}kb/s
                    <# if ( data.meta.bitrate_mode ) { #>
                    {{ ' ' + data.meta.bitrate_mode.toUpperCase() }}
                    <# } #>
                </div>
                <# } #>
                <div class="compat-meta">
                    <# if ( data.compat && data.compat.meta ) { #>
                    {{{ data.compat.meta }}}
                    <# } #>
                </div>
            </div>
            <div class="settings">
                <label class="setting" data-setting="url">
                    <span class="name"><?php
                        _e('URL'); ?></span>
                    <input type="text" value="{{ data.url }}" readonly/>
                </label>
                <# var maybeReadOnly = data.can.save || data.allowLocalEdits ? '' : 'readonly'; #>
                <label class="setting" data-setting="title">
                    <span class="name"><?php
                        _e('Title'); ?></span>
                    <input type="text" value="{{ data.title }}" {{ maybeReadOnly }}/>
                </label>
                <# if ( 'audio' === data.type ) { #>
                <?php
                foreach (
                    [
                        'artist' => __('Artist'),
                        'album' => __('Album'),
                    ] as $key => $label
                ) : ?>
                    <label class="setting" data-setting="<?php
                    echo esc_attr($key) ?>">
                        <span class="name"><?php
                            echo $label ?></span>
                        <input type="text" value="{{ data.<?php
                        echo $key ?> || data.meta.<?php
                        echo $key ?> || '' }}"/>
                    </label>
                <?php
                endforeach; ?>
                <# } #>
                <# if ( 'image' === data.type ) { #>
                <label class="setting" data-setting="caption">
                    <span class="name"><?php
                        _e('Autor'); ?></span>
                    <textarea {{ maybeReadOnly }}>{{ data.caption }}</textarea>
                </label>
                <# } #>
                <!-- LET'S REMOVE THIS SECTION:
                                <# if ( 'image' === data.type ) { #>
                                        <label class="setting" data-setting="alt">
                                                <span class="name"><?php
                _e('Alt Text'); ?></span>
                                                <input type="text" value="{{ data.alt }}" {{ maybeReadOnly }} />
                                        </label>
                                <# } #>
                                <label class="setting" data-setting="description">
                                        <span class="name"><?php
                _e('Description xxx'); ?></span>
                                        <textarea {{ maybeReadOnly }}>{{ data.description }}</textarea>
                                </label>
                                <label class="setting">
                                        <span class="name"><?php
                _e('Uploaded By'); ?></span>
                                        <span class="value">{{ data.authorName }}</span>
                                </label>
                                <# if ( data.uploadedToTitle ) { #>
                                        <label class="setting">
                                                <span class="name"><?php
                _e('Uploaded To'); ?></span>
                                                <# if ( data.uploadedToLink ) { #>
                                                        <span class="value"><a href="{{ data.uploadedToLink }}">{{ data.uploadedToTitle }}</a></span>
                                                <# } else { #>
                                                        <span class="value">{{ data.uploadedToTitle }}</span>
                                                <# } #>
                                        </label>
                                <# } #>
-->
                <div class="attachment-compat"></div>
            </div>
            <div class="actions">
                <a class="view-attachment" href="{{ data.link }}"><?php
                    _e('View attachment page'); ?></a>
                <# if ( data.can.save ) { #> |
                <a href="post.php?post={{ data.id }}&action=edit"><?php
                    _e('Edit more details'); ?></a>
                <# } #>
                <# if ( ! data.uploading && data.can.remove ) { #> |
                <?php
                if (MEDIA_TRASH): ?>
                    <# if ( 'trash' === data.status ) { #><a class="untrash-attachment" href="#"><?php
                        _e('Untrash'); ?></a><# } else { #><a class="trash-attachment" href="#"><?php
                        _e('Trash'); ?></a><# } #>
                <?php
                else: ?>
                    <a class="delete-attachment" href="#"><?php
                        _e('Delete Permanently'); ?></a>
                <?php
                endif; ?>
                <# } #>
            </div>
        </div>
    </script>
    <script>
        jQuery(document).ready(function ($) {
            if (typeof wp.media.view.Attachment.Details.TwoColumn != 'undefined') {
                wp.media.view.Attachment.Details.TwoColumn.prototype.template = wp.template('attachment-details-two-column-custom');
            }
        });
    </script>
    <?php
}

add_action('admin_head-post.php', 'hideMediaUploaderFields');
add_action('admin_head-post-new.php', 'hideMediaUploaderFields');
add_action('admin_footer-upload.php', 'hideMediaUploaderFields');

function hideMediaUploaderFields()
{
    ?>
    <style type="text/css">
        .smush-stats,
        .media-types-required-info,
        .compat-field-regenerate_thumbnails,
        .compat-field-wio {
            display: none !important;
        }
    </style>
    <?php
}

remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10);

// remove dashicons in frontend to non-admin
function wpdocs_dequeue_dashicon()
{
    if (current_user_can('update_core')) {
        return;
    }
    wp_deregister_style('dashicons');
}

/**
 * Getting all ids from ES, and then using mysql to scroll for items, in order to make sure there is enough results in
 * each iteration.
 * @todo Could be optimized to send less data to mysql.
 *
 */
function attachmentCustomSearch()
{
    global $wpdb;
    if (!current_user_can('upload_files')) {
        wp_send_json_error();
    }
    $perPage = $_REQUEST['query']['posts_per_page'];
    $page = $_REQUEST['query']['paged'];
    $offset = ((int)$page - 1) * (int)$perPage;
    // @TODO implement date search
    if (isset($_REQUEST['year'], $_REQUEST['monthnum'])) {
        $year = $_REQUEST['year'];
        $month = $_REQUEST['monthnum'];
    }
    //First time media uploader is loaded get attachments without any filter
    $imageIds = '1';

    //If search is set in Request
    if (isset($_REQUEST['query']['s'])) {
        $value = $_REQUEST['query']['s'];
        $sortedItems = ImageRepo::elasticImageSearch($value, 10000);
        $ids = [];
        foreach ($sortedItems['images'] as $key => $imageData) {
            $ids[] = $imageData['postId'];
        }
        if (count($ids)) {
            $imageIds = implode(',', $ids);
        }
        $sql = "SELECT * FROM $wpdb->posts WHERE ID IN (" . $imageIds . ") AND post_type = 'attachment' ORDER BY post_date DESC LIMIT $perPage OFFSET $offset";
    } else {
        $sortedItems = ImageRepo::elasticImageSearch('', 10000);
        $ids = [];
        foreach ($sortedItems['images'] as $key => $imageData) {
            $ids[] = $imageData['postId'];
        }
        if (count($ids)) {
            $imageIds = implode(',', $ids);
        }
        $sql = "SELECT * FROM $wpdb->posts WHERE ID IN (" . $imageIds . ") AND post_type = 'attachment' ORDER BY post_date DESC LIMIT $perPage OFFSET $offset";
    }
//    var_dump($query);
//    die();
    $attachmentsDb = $wpdb->get_results($sql, OBJECT);
    $attachments = array_map('wp_prepare_attachment_for_js', $attachmentsDb);
    $attachments = array_filter($attachments);
    wp_send_json_success($attachments);
}

remove_action('wp_ajax_query-attachments', 'wp_ajax_query_attachments', 1);
remove_action('wp_ajax_nopriv_query-attachments', 'wp_ajax_query_attachments', 1);

add_action('wp_ajax_query-attachments', 'attachmentCustomSearch', 1); // for logged in user
add_action('wp_ajax_nopriv_query-attachments', 'attachmentCustomSearch', 1); // if user not logged in

//Facebook
//add_filter( 'rank_math/opengraph/image_sizes', function( $sizes ) {
//    return ['full'];
//});

add_filter(
    'rank_math/opengraph/facebook/image',
    static function () {
        global $post;
        $imageUrl = wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'full');
        if ($imageUrl === '') {
            $imageUrl = wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'single');
        }
        return $imageUrl;
    }
);

add_filter('posts_search', 'customSearch', 500, 2);
function customSearch($search, $query)
{
    global $wpdb;
    if (empty($search)) {
        return $search;
    }

    $terms = $query->query_vars['s'];
    $search = ' AND wp_posts.ID IN ';
    $limit = 500;
    $sortedItems = ArticleRepo::elasticSearch($terms, 1, $limit);
    $offset = 0;
    $ids = [];
    /* @var \GfWpPluginContainer\Wp\Article $article */
    foreach ($sortedItems['articles'] as $key => $article) {
        if ($key >= $offset && $key < ($offset + $limit)) {
            $ids[] = $article->getPostId();
        }
        if ($key > ($offset + $limit)) {
            break;
        }
    }
    $ids = implode(',', $ids);
    // @TODO wordpress executes some strange query with ALL post ids in single query !?
    if (strlen($ids) === 0) {
        $ids = 1;
    }
    $search .= "({$ids})";

    return $search;
}

add_action('admin_print_styles', 'hideStickyOption');
function hideStickyOption()
{
    $author = false;
    $editor = false;
    if (checkForLoggedInUserRole('author')) {
        $author = true;
    }
    if (checkForLoggedInUserRole('editor')) {
        $editor = true;
    }
    global $post_type, $pagenow;
    if ('post.php' !== $pagenow && 'post-new.php' !== $pagenow) {
        return;
    }
    ?><?php
    if ($author): // For authors
        ?>
        <style type="text/css">
            .edit-post-post-status .components-panel__row:nth-of-type(1),
            .edit-post-post-status .components-panel__row:nth-of-type(3),
            .edit-post-post-status .components-panel__row:nth-of-type(5),
            .edit-post-post-status .components-panel__row:nth-of-type(6),
            .edit-post-post-status .components-panel__row:nth-of-type(7),
            #wp2t,
            #A2A_SHARE_SAVE_meta {
                display: none;
            }

            <?php if($pagenow === 'post.php') :// On edit post admin page for authors ?>
            <?php if (get_post_status() === 'publish'):?>
            .edit-post-post-status .components-panel__row:nth-of-type(4),
            <?php endif; ?>
            .edit-post-sidebar .components-panel .components-panel__body:nth-of-type(7),
            .edit-post-sidebar .components-panel .components-panel__body:nth-of-type(8) {
                display: none !important;
            }

            <?php endif; ?>
            <?php if($pagenow === 'post-new.php') :// On new post admin page for authors ?>
            .edit-post-sidebar .components-panel .components-panel__body:nth-of-type(6),
            .edit-post-sidebar .components-panel .components-panel__body:nth-of-type(7),
            .edit-post-sidebar .components-panel .components-panel__body:nth-of-type(8) {
                display: none !important;
            }

            <?php endif; ?>
        </style>
    <?php
    endif ?><?php
    if ($editor): // For editors
        ?>
        <style type="text/css">
            .edit-post-post-status .components-panel__row:nth-of-type(1),
            .edit-post-post-status .components-panel__row:nth-of-type(3),
            .edit-post-post-status .components-panel__row:nth-of-type(5),
            .edit-post-post-status .components-panel__row:nth-of-type(6),
            .edit-post-post-status .components-panel__row:nth-of-type(7),
            .edit-post-sidebar .components-panel .components-panel__body:nth-of-type(7),
            #wp2t,
            #A2A_SHARE_SAVE_meta {
                display: none;
            }
            <?php if($pagenow === 'post.php') :// On edit post admin page for editors ?>
            <?php if (get_post_status() === 'publish'):?>
            .edit-post-post-status .components-panel__row:nth-of-type(4){
                display: none;
            }
            <?php endif; ?>
            <?php endif?>
        </style>
    <?php
    endif;
}

// Enqueue a script that disables core gutenberg blocks defined in the src script
function unregisterCoreBlocks()
{
    wp_enqueue_script(
        'my-plugin-deny-list-blocks',
        CHILD_THEME_DIR_URI . '/js/blocks/unregisterBlock.js',
        ['wp-blocks', 'wp-dom-ready', 'wp-edit-post']
    );
}

add_action('enqueue_block_editor_assets', 'unregisterCoreBlocks');


// Give authors the permission to edit attachment files
function attachmentPermissions($userCaps, $requiredCap, $args)
{
    // Return if it is not a post
    if ('edit_post' !== $args[0]) {
        return $userCaps;
    }

    // Return if the user can already edit others' posts
    if ($userCaps['edit_others_posts']) {
        return $userCaps;
    }

    // Return if the user can't publish posts
    if (!isset($userCaps['publish_posts']) && !$userCaps['publish_posts']) {
        return $userCaps;
    }

    // Get the post ($args[2] is the object that's queried and  that we pass to the get_post function)
    $post = get_post($args[2]);

    // Return if the user is the author of the post ($args[1] gives us the user ID of the queried post)
    if ($args[1] === $post->post_author) {
        return $userCaps;
    }

    // Return if the requested post type is not an attachment
    if ('attachment' !== $post->post_type) {
        return $userCaps;
    }

    // If the required capability is edit post, set the user's capability for the requested (required) capability to true
    if ('edit_post' === $args[0]) {
        $userCaps[$requiredCap[0]] = true;
    }
    return $userCaps;
}

add_filter('user_has_cap', 'attachmentPermissions', 10, 3);

// Hide certain options for editors and authors in the frontend admin bar
if (checkForLoggedInUserRole('editor') || checkForLoggedInUserRole('author')) {
    WpEnqueue::addFrontendStyle('frontEndAdminBar', PARENT_THEME_DIR_URI . '/css/frontEndAdmin/frontEndAdminBar.css');
}

// Add 'FOTO: ' before every image caption ( for new images )
add_action('add_attachment', 'defaultCaptionPrefix');
function defaultCaptionPrefix($post_id)
{
    if (wp_attachment_is_image($post_id)) {
        $imageCaptionPrefix = 'FOTO: ';

        $imageMeta = [
            'ID' => $post_id,
            'post_excerpt' => $imageCaptionPrefix,
        ];
        wp_update_post($imageMeta);
    }
}

// This doesn't update the field immediately on the front, for now.
//function defaultCaptionOnUpdate($post, $attachment) {
//    if ( substr($post['post_mime_type'], 0, 5) === 'image' ) {
//        if (trim($post['post_excerpt']) === '') {
//            $post['post_excerpt'] = 'FOTO: ';
//        }
//        elseif (substr($post['post_excerpt'],0,6) !=='FOTO: ') {
//            $post['post_excerpt'] = 'FOTO: ' . $post['post_excerpt'];
//        }
//    }
//    return $post;
//}
//
//add_filter('attachment_fields_to_save', 'defaultCaptionOnUpdate', 10, 2);

/*  Filter the 'author' input on edit attachment, saves to the database,
    (refresh required on the frontend to see the change in the admin)
    Remove and add action within the function stops the media uploader to crash when uploading new images */
// TODO see why the "author" field updates realtime when 'legenda' is changed but not vice versa
add_action('edit_attachment', 'updateCaptionOnEditAttachment', 10, 5);
function updateCaptionOnEditAttachment($post_id)
{
    remove_action('edit_attachment', 'updateCaptionOnEditAttachment');
    $post = get_post($post_id);
    $postCaption = $post->post_excerpt;
    if (trim($postCaption) === '') {
        $postCaption = 'FOTO: ';
    } elseif (substr($postCaption, 0, 6) !== 'FOTO: ') {
        $postCaption = 'FOTO: ' . $postCaption;
    }
    $imageMeta = [
        'ID' => $post_id,
        'post_excerpt' => $postCaption,
    ];
    wp_update_post($imageMeta);
    add_action('edit_attachment', 'updateCaptionOnEditAttachment');
}

add_filter('manage_posts_columns', 'addViewCount');
function addViewCount($defaults)
{
    $defaults['viewCount'] = 'Broj pregleda';
    return $defaults;
}

add_action('manage_posts_custom_column', 'addViewCountValue', 10, 2);
function addViewCountValue($column_name, $post_ID)
{
    if ($column_name === 'viewCount') {
        global $cache;

        $count = $cache->get('gfPostViewCount#' . $post_ID);
        if (!$count) {
//            $count = get_post_meta($post_ID, 'gfPostViewCount', true);
        }
        echo $count;
    }
}

// @TODO quickfix
function defaultFeaturedImage()
{
}


add_filter('rest_post_dispatch', 'testApi2', 5, 3);
// a fix for
function testApi2(WP_HTTP_Response $wpResponse, WP_REST_Server $wpServer, WP_REST_Request $wpRequest)
{
    if ($wpRequest->get_route() === '/wp/v2/tags') {
        // params to include when this is moved to ES
//        $per_page = isset($wpRequest->get_query_params()['per_page']) ? $wpRequest->get_query_params()['per_page'] : 20;
//        $orderBy = isset($wpRequest->get_query_params()['orderBy']) ? $wpRequest->get_query_params()['orderBy'] : 'date';
//        $order = isset($wpRequest->get_query_params()['order']) ? $wpRequest->get_query_params()['order'] : 'desc';
//        $include = isset($wpRequest->get_query_params()['include']) ? $wpRequest->get_query_params()['include'] : false;
//        $fields = isset($wpRequest->get_query_params()['_fields']) ? $wpRequest->get_query_params()['_fields'] : 'id,name';

        $wpResponse->headers['X-WP-Total'] = 100;
        $wpResponse->headers['X-WP-TotalPages'] = 1;
        $wpResponse->headers['Link'] = '';
    }

    return $wpResponse;
}

add_action('admin_footer-upload.php', 'modifyMediaThumbnailCss');
function modifyMediaThumbnailCss()
{
    echo '<style>
    .media-frame-content[data-columns="11"] .attachment {
        width: 20%;
    }
    .wp-core-ui .attachment .thumbnail .centered img {
        width:100%;
    }
</style>';
}

function cropBlicImage($id) {
    $attachment = get_post($id);
    $imageURI = $attachment->guid;

    $path = get_attached_file( $attachment->ID );
    $filename = basename ( get_attached_file( $attachment->ID ) );
    $path = str_replace($filename, "", $path);

    $image_size = '160x213';
    $img_temp = explode("x", $image_size);
    $img_width = $img_temp[0];
    $img_height = $img_temp[1];

    $filename_explode = explode(".", $filename);
    $filename_name = $filename_explode[0];
    $filename_ext = $filename_explode[1];
    $imageURI_temp = str_replace($filename, "", $imageURI);
    $exist = $path . $filename_name.'-'.$image_size.'.'.$filename_ext;

    if(!file_exists($exist)):
        $img_path = $path;
        $img_name = $filename;
        $image = wp_get_image_editor( $img_path . $img_name );
        if ( ! is_wp_error( $image ) ) {
            $image->resize( $img_width, $img_height, true );
            $image->save( $img_path . $image_size.'-'-$filename );
            $final_image = $imageURI_temp . $filename_name .'-'.$image_size.'.'.$filename_ext;

            return $final_image;
        }
    endif;

    return $imageURI_temp . $filename_name.'-'.$image_size.'.'.$filename_ext;
}