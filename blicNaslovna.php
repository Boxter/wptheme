<?php
get_header();
//Template Name: Blic Naslovna
//Takes value from homepage block blic naslovna
$dotMetricsId = 4595;
$id = get_option('blicNaslovnaImg');
$imgSrcDesktop = wp_get_attachment_image_src($id,'euro-blic-d')[0];
if (wp_is_mobile()){
    $imgSrcMobile = wp_get_attachment_image_src($id,'euro-blic-m')[0];
}
?>
    <div class="container">
        <div class="siCategory">
            <div class="category__left">
                <div>
                    <?php if(wp_is_mobile()) : ?>
                        <img id="myImg" src="<?=$imgSrcMobile?>" data-image-large="<?=$imgSrcDesktop?>" alt="blic naslovna" title="blic naslovna">
                        <div id="myModal" class="modal">
                            <span class="close">&times;</span>
                            <img class="modal-content" id="img01">
                        </div>
                    <?php else : ?>
                        <img class="blicNaslovnaPage" src="<?=$imgSrcDesktop?>" title="blic naslovna" alt="blic naslovna">
                    <?php endif; ?>
                </div>
            </div>
            <aside class="category__right"> <!-- Category Right Side -->
				<?php dynamic_sidebar( 'blic_naslovna_sidebar' ); ?>
            </aside><!-- Category Right Side End -->
        </div>
    </div>

<?php if(wp_is_mobile()) : ?>
    <script type="text/javascript" src="<?= PARENT_THEME_DIR_URI . '/js/blicNaslovnaPopUp/blicNaslovnaPopUp.js'?>"></script>
<?php endif; ?>
<?php
get_footer();