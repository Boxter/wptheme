<?php
/* Template Name: Category Page */

use \GfWpPluginContainer\Indexer\Repository\Article as ArticleRepo;

get_header();
$paged = (get_query_var('paged') > 0) ? get_query_var('paged') : 1;
$catName = (isset($_GET['q'])) ? explode('/', $_GET['q'])[1] : '';
if (strlen($catName) === 0) {
    $catName = explode('/', $_SERVER['REQUEST_URI'])[1];
}
$cat = get_term_by('slug', $catName, 'category');
$catName = $cat->name;
$catUrl = '/' . $cat->slug . '/';
$dotMetricsId = getDotMetricsId($cat->slug);
//Needed for infinite scroll
$ajaxAction = 'category';
$ajaxTermValue = $catName;

//$searchFunctions = new \GfWpPluginContainer\Elastic\Functions($wpdb);
if (USE_ELASTIC !== true) {
    $sortedItems = ArticleRepo::getItemsFromWp([
        'category_name' => $catName,
        'paged'=> $paged,
        'numberposts' => PER_PAGE
    ]);
} else {
    $sortedItems = ArticleRepo::getItemsFromElasticBy('category', $cat, $paged, PER_PAGE);
}

$lastPage = ceil( $sortedItems['totalCount'] / PER_PAGE);
if ($paged > $lastPage) {
    $paged = $lastPage;
    wp_safe_redirect(get_permalink(). 'page/'.$lastPage,302);
}

if ($isApp) {
    include "templates/archive/archiveMobileApp.php";
    wp_footer();
} else {
    include "templates/archive/archiveDesktop.php";
	get_footer();
}
