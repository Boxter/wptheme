<?php
/* Template Name: All posts */

use \GfWpPluginContainer\Indexer\Repository\Article as ArticleRepo;

get_header();
$paged = (get_query_var('paged') > 0) ? get_query_var('paged') : 1;
$catName = 'Sve vijesti';
$catUrl = 'sve-vijesti';
$dotMetricsId = getDotMetricsId($catUrl);
//Needed for infinite scroll
$ajaxAction = 'newPosts';
$ajaxTermValue = $catName;

//$searchFunctions = new \GfWpPluginContainer\Elastic\Functions($wpdb);
if (USE_ELASTIC !== true) {
    $sortedItems = ArticleRepo::getItemsFromWp([
        'paged'=> $paged,
        'posts_per_page' => PER_PAGE
    ]);
} else {
    $termMock = new \Stdclass();
    $termMock->term_id = 0;
    $sortedItems = ArticleRepo::getItemsFromElasticBy($catUrl, $termMock, $paged, PER_PAGE);
}
if ($isApp) {
    include "templates/archive/archiveMobileApp.php";
    wp_footer();
} else {
    include "templates/archive/archiveDesktop.php";
	get_footer();
}
