<!DOCTYPE html>
<html lang="sr">
<head>
    <link rel="stylesheet" href="https://cdn.consentmanager.mgr.consensu.org/delivery/cmp.min.css" />
    <script>window.gdprAppliesGlobally=true;if(!("cmp_id" in window)){window.cmp_id=21173}if(!("cmp_params" in window)){window.cmp_params=""}window.cmp_host="consentmanager.mgr.consensu.org";window.cmp_cdn="cdn.consentmanager.mgr.consensu.org";function cmp_getlang(j){if(typeof(j)!="boolean"){j=true}if(j&&typeof(cmp_getlang.usedlang)=="string"&&cmp_getlang.usedlang!==""){return cmp_getlang.usedlang}var g=["DE","EN","FR","IT","NO","DA","FI","ES","PT","RO","BG","ET","EL","GA","HR","LV","LT","MT","NL","PL","SV","SK","SL","CS","HU","RU","SR","ZH","TR","UK"];var c=[];var f=location.hash;var e=location.search;var a="languages" in navigator?navigator.languages:[];if(f.indexOf("cmplang=")!=-1){c.push(f.substr(f.indexOf("cmplang=")+8,2))}else{if(e.indexOf("cmplang=")!=-1){c.push(e.substr(e.indexOf("cmplang=")+8,2))}else{if("cmp_setlang" in window&&window.cmp_setlang!=""){c.push(window.cmp_setlang.toUpperCase())}else{if(a.length>0){for(var d=0;d<a.length;d++){c.push(a[d])}}}}}if("language" in navigator){c.push(navigator.language)}if("userLanguage" in navigator){c.push(navigator.userLanguage)}var h="";for(var d=0;d<c.length;d++){var b=c[d].toUpperCase();if(b.indexOf("-")!=-1){b=b.substr(0,2)}if(g.indexOf(b)!=-1){h=b;break}}if(h==""&&typeof(cmp_getlang.defaultlang)=="string"&&cmp_getlang.defaultlang!==""){return cmp_getlang.defaultlang}else{if(h==""){h="EN"}}h=h.toUpperCase();return h}(function(){var a="";var c="_en";if("cmp_getlang" in window){a=window.cmp_getlang().toLowerCase();c="_"+a}var d=("cmp_ref" in window)?window.cmp_ref:location.href;var b=document.createElement("script");b.src="https://"+window.cmp_host+"/delivery/cmp.php?id="+window.cmp_id+"&h="+encodeURIComponent(d)+"&"+window.cmp_params+(document.cookie.length>0?"&__cmpfcc=1":"")+"&l="+a+"&o="+(new Date()).getTime();b.type="text/javascript";b.setAttribute("data-cmp-ab","1");b.async=true;if(document.body){document.body.appendChild(b)}else{if(document.currentScript){document.currentScript.parentElement.appendChild(b)}else{document.write(b.outerHTML)}}var b=document.createElement("script");b.src="https://"+window.cmp_cdn+"/delivery/cmp"+c+".min.js";b.type="text/javascript";b.setAttribute("data-cmp-ab","1");b.async=true;if(document.body){document.body.appendChild(b)}else{if(document.currentScript){document.currentScript.parentElement.appendChild(b)}else{document.write(b.outerHTML)}}window.cmp_addFrame=function(f){if(!window.frames[f]){if(document.body){var e=document.createElement("iframe");e.style.cssText="display:none";e.name=f;document.body.appendChild(e)}else{window.setTimeout('window.cmp_addFrame("'+f+'")',10)}}};window.cmp_rc=function(l){var e=document.cookie;var j="";var g=0;while(e!=""&&g<100){g++;while(e.substr(0,1)==" "){e=e.substr(1,e.length)}var k=e.substring(0,e.indexOf("="));if(e.indexOf(";")!=-1){var f=e.substring(e.indexOf("=")+1,e.indexOf(";"))}else{var f=e.substr(e.indexOf("=")+1,e.length)}if(l==k){j=f}var h=e.indexOf(";")+1;if(h==0){h=e.length}e=e.substring(h,e.length)}return(j)};window.cmp_stub=function(){var e=arguments;__cmapi.a=__cmapi.a||[];if(!e.length){return __cmapi.a}else{if(e[0]==="ping"){if(e[1]===2){e[2]({gdprApplies:gdprAppliesGlobally,cmpLoaded:false,cmpStatus:"stub",displayStatus:"hidden",apiVersion:"2.0",cmpId:31},true)}else{e[2]({gdprAppliesGlobally:gdprAppliesGlobally,cmpLoaded:false},true)}}else{if(e[0]==="getUSPData"){e[2]({version:1,uspString:window.cmp_rc("")},true)}else{if(e[0]==="getTCData"){__cmapi.a.push([].slice.apply(e))}else{if(e[0]==="addEventListener"){__cmapi.a.push([].slice.apply(e))}else{if(e.length==4&&e[3]===false){e[2]({},false)}else{__cmapi.a.push([].slice.apply(e))}}}}}}};window.cmp_msghandler=function(j){var f=typeof j.data==="string";try{var h=f?JSON.parse(j.data):j.data}catch(k){var h=null}if(typeof(h)==="object"&&h!==null&&"__cmpCall" in h){var g=h.__cmpCall;window.__cmp(g.command,g.parameter,function(l,i){var e={__cmpReturn:{returnValue:l,success:i,callId:g.callId}};j.source.postMessage(f?JSON.stringify(e):e,"*")})}if(typeof(h)==="object"&&h!==null&&"__cmapiCall" in h){var g=h.__cmapiCall;window.__cmapi(g.command,g.parameter,function(l,i){var e={__cmapiReturn:{returnValue:l,success:i,callId:g.callId}};j.source.postMessage(f?JSON.stringify(e):e,"*")})}if(typeof(h)==="object"&&h!==null&&"__uspapiCall" in h){var g=h.__uspapiCall;window.__uspapi(g.command,g.version,function(l,i){var e={__uspapiReturn:{returnValue:l,success:i,callId:g.callId}};j.source.postMessage(f?JSON.stringify(e):e,"*")})}if(typeof(h)==="object"&&h!==null&&"__tcfapiCall" in h){var g=h.__tcfapiCall;window.__tcfapi(g.command,g.version,function(l,i){var e={__tcfapiReturn:{returnValue:l,success:i,callId:g.callId}};j.source.postMessage(f?JSON.stringify(e):e,"*")},g.parameter)}};window.cmp_setStub=function(e){if(!(e in window)||(typeof(window[e])!=="function"&&typeof(window[e])!=="object"&&(typeof(window[e])==="undefined"||window[e]!==null))){window[e]=window.cmp_stub;window[e].msgHandler=window.cmp_msghandler;if(window.addEventListener){window.addEventListener("message",window.cmp_msghandler,false)}else{window.attachEvent("onmessage",window.cmp_msghandler)}}};window.cmp_addFrame("__cmapiLocator");window.cmp_addFrame("__cmpLocator");window.cmp_addFrame("__uspapiLocator");window.cmp_addFrame("__tcfapiLocator");window.cmp_setStub("__cmapi");window.cmp_setStub("__cmp");window.cmp_setStub("__tcfapi");window.cmp_setStub("__uspapi")})();</script>
    <?php
    global $isApp;

    $homeUrl    = '';
    //Get permalink for the current page
    $objId      = get_queried_object_id();
    $currentUrl = get_permalink( $objId );
    $searchUrl = '/pretraga/';
    ?>
    <meta property="fb:admins" content="boris.lakic" />
    <meta property="fb:admins" content="branko.tomic65" />
    <meta property="fb:app_id" content="206933876746107" />
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <?php if ($isApp):
        $searchUrl = '/app/';
    ?><meta name="MobileOptimized" content="320">
    <meta name="HandheldFriendly" content="True">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">
    <?php else :?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php endif;?>
    <link rel="dns-prefetch" href="//dotmetrics.net" />
    <link rel="dns-prefetch" href="//google-analytics.com" />
    <link rel="dns-prefetch" href="//googletagservices.com" />

    <?php wp_head(); ?>
    <!-- Google tag/ads -->
    <script async='async' src='https://securepubads.g.doubleclick.net/tag/js/gpt.js'></script>
    <meta name="google-site-verification" content="GMR8sFRTs8ryNiP4W9ZDAep5Ko8frVczQ2DLqDZvyEk" />
    <script>
        var gptadslots = [];
        var googletag = googletag || {cmd:[]};
    </script>
    <script>
        <?=getAdsScript($currentUrl)?>
    </script>
    <!-- /Google tag/ads -->
    <link rel="manifest" href="/manifest.json" />
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(function() {
            OneSignal.init({
                appId: "b41623da-e2f2-4bb7-ad9b-3be7f12254c8",
                autoRegister: true,
                notifyButton: {
                    enable: false,
                },
            });
        });
    </script>
</head>
<body <?php body_class(); ?>>
<?php $isMobile = (int) wp_is_mobile();?>
<script>
    let isMobile = "<?=$isMobile?>";
    let isApp = "<?=$isApp?>";
</script>
<?php if($isApp):?>
    <div class="loadingWrapper">

        <div class="loading">

        </div>
    </div>
<?php endif;?>
<div class="overlay"></div>
<aside class="aside-menu">
    <div class="aside-menu__top">
        <span class="aside-menu__close"><i class="fas fa-times"></i></span>
        <form method="get" class="aside-menu__search" action="<?=$searchUrl?>">
            <?php if ($isApp):?>
            <input class="searchInput" type="search" name="url" value="" placeholder="Pretraga" aria-label="search" />
            <input type="hidden" name="type" value="search"/>
            <?php else: ?>
            <input class="searchInput" type="search" name="query" value="" placeholder="Pretraga" aria-label="search" />
            <?php endif?>
            <button class="buttonSubmitSearch" type="submit" value="search" aria-label="search"><i class="fas fa-search"></i></button>
        </form>
    </div>
    <div class="aside-menu__scroll">
        <nav>
            <ul>
                <?php
                global $cache;
                $key = 'navigation-mobile#Main Navigation Mobile';
                if ($isApp){
	                $key = 'navigation-mobile#Main Navigation Mobile App';
                }
                $html = $cache->get($key);
                if ($html === false) {
                    $menu = wp_get_nav_menu_items('Main Navigation Mobile');
                    /** @var WP_Post $menuItem */
                    $html = '';
                    foreach ($menu as $menuItem) {
                        $menuItemUrl = $menuItem->url;
                        if ($isApp) {
                            if ($menuItem->title === 'Početna') {
                                $menuItemUrl = parseAppUrl('page', 'pocetna');
                            } else {
                                $menuItemUrl = parseAppUrl('page', $menuItem->url);
                            }
                        }
                        if ($menuItem->title !== 'Digitalna Srpska') {
                            $html .= '<li><a title="' . esc_attr($menuItem->title) . '" href="' . $menuItemUrl . '">' . $menuItem->title . '</a></li>';
                        }
                        else {
                            $html .= '<li class="sponsor">
                                        <a title="Digitalna Srpska" href="' . $menuItemUrl . '">Digitalna Srpska
                                            <div class="sponsor__logo"><span>By</span> <img src="' . CHILD_THEME_DIR_URI . '/images/logo-mtel.svg' . '" alt="By Mtel" width="40" height="40" /></div>
                                        </a>
                                    </li>';
                        }
                    }
                    $cache->set($key, $html, 300);
                }
                echo $html;
                ?>
            </ul>
        </nav>
    </div>
</aside>

<?php
if ($isApp) {
    include("templates/header/view/headerMobile.php");
} else {
    include("templates/header/view/header.php");
}
get_sidebar();
?>
