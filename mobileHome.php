<?php
/**
 * Template Name: Mobile Home
 *
 */

//header('Location: ' . home_url() . '/?isapp=true', 302); // tmp
//exit();
$dotMetricsId = 3934;

$frontPageObject = get_post(get_option('page_on_front'));
if ($frontPageObject) {
    get_header();
    $blocks = parse_blocks($frontPageObject->post_content);
    foreach ($blocks as $block) {
        echo apply_filters('the_content', render_block($block));
    } ?>
    <?php
    get_footer();
}