//See functions.php:110 for list of supported actions
jQuery(document).ready(function ($) {

    function infiniteScrollInit(totalPages, ajaxUrl, action) {
        let infiniteLoader = $('#infiniteLoader')

        if (infiniteLoader.length > 0) {
            //Page is 1 is loaded on first request so the page count starts from 2
            let page = 2;
            let total = totalPages;
            $(window).scroll(function () {
                console.log('radim');
                if ($(window).scrollTop() === $(document).height() - $(window).height()) {
                    if (page > total) {
                        return false;
                    } else {
                        loadArticle(ajaxUrl, action, page);
                    }
                    page++;
                }
            });
        }
    }

    function loadArticle(ajaxUrl, termValue, page) {
        //@todo implement loader
        //Show loader
        $('#inifiniteLoader').show('fast');

        $.ajax({
            url: ajaxUrl,
            type: 'POST',
            data: 'action=infinite_scroll&page=' + page + '&action=' + termValue + '&value=' + termValue,
            success: function (html) {
                //Hide loader
                $('#inifiniteLoader').hide('1000');
                $('.items__2').append(html);
            }
        });
        return false;
    }
});