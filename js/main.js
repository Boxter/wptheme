"use strict";

// sticky header
var body = document.body;
var hidden = "hidden";
var lastScroll = 0; // media queries
var ajaxUrl; // infinite scroll url

var desktopUp = window.matchMedia("(min-width: 1365px)");
window.addEventListener("scroll", function () {
    var currentScroll = window.pageYOffset;

    if (desktopUp.matches) {
        // sticky header
        if (currentScroll > lastScroll && !body.classList.contains(hidden)) {
            // down
            if (currentScroll > 200) {
                body.classList.add(hidden);
            }
        } else if (currentScroll < lastScroll && body.classList.contains(hidden)) {
            // up
            body.classList.remove(hidden);
        }
    }

    lastScroll = currentScroll;
}); // toggle aside menu
var menu = document.querySelector('.menu__button');
var asideMenu = document.querySelector('.aside-menu');
var asideMenuClose = document.querySelector('.aside-menu__close');
var overlay = document.querySelector('.overlay');
menu.addEventListener('click', function () {
    asideMenu.classList.add('active');
    overlay.classList.add('active');
    if (stickyBlock !== null) {
        stickyBlock.classList.remove('banner--sticky'); // hide the sticky billboard banner on mobile when menu is open
    }
});
asideMenuClose.addEventListener('click', function () {
    asideMenu.classList.remove('active');
    overlay.classList.remove('active');
});
overlay.addEventListener('click', function () {
    if (this.classList.contains('active')) {
        this.classList.remove('active');
        asideMenu.classList.remove('active');
    }
});


//     GUTNEBERG BLOCKS

// Fix columns for layout
document.querySelectorAll('.homepageNews').forEach(function (item) {
    item.querySelector('.wp-block-columns').classList.add('box');
    item.querySelector('.wp-block-columns').classList.remove('wp-block-columns');
});

// find every wp-block-column class PAIR and change their classes ( hence el.length / 2 )
let el = document.getElementsByClassName('wp-block-column');
let num = el.length / 2;
for (let i = 0; i < num; i++) {
    document.querySelector(".wp-block-column").classList.add('box__left');
    document.querySelector(".wp-block-column").classList.remove('wp-block-column');

    document.querySelector(".wp-block-column").classList.add('box__right');
    document.querySelector(".wp-block-column").classList.remove('wp-block-column');
}


function showLatestPosts() {
    let latestPosts = document.querySelector('.gfLatestNews');
    let popularPosts = document.querySelector('.gfPopularNews');

    popularPosts.classList.add('gfHidden');
    popularPosts.classList.remove('gfShow');


    latestPosts.classList.add('gfShow');
    latestPosts.classList.remove('gfHidden');
}

function showPopularPosts() {
    let latestPosts = document.querySelector('.gfLatestNews');
    let popularPosts = document.querySelector('.gfPopularNews');

    latestPosts.classList.add('gfHidden');
    latestPosts.classList.remove('gfShow');

    popularPosts.classList.add('gfShow');
    popularPosts.classList.remove('gfHidden');

}

//     GUTNEBERG BLOCKS  END


// Banner mobile billboard sticky
let stickyBlock = document.querySelector('.banner--sticky_billboard'); // sticky billboard on mobile
let overlayB = jQuery('.overlay'); // overlay
let overlayIsActive = overlayB.hasClass('active');
jQuery(window).scroll(function(){
    if(jQuery(window).scrollTop() > 400 || overlayIsActive === true ){
        jQuery(stickyBlock).removeClass("banner--sticky");
    }
});
jQuery(window).scroll(function(){
    if(jQuery(window).scrollTop() < 400){
        if (overlayB.hasClass('active') === true) {
            return;
        }
        jQuery(stickyBlock).addClass("banner--sticky");
    }
});

// Empty search placeholder on menu close
jQuery(document).ready(function(){
        let form = jQuery('.aside-menu__search');
        jQuery(asideMenuClose).add(overlayB).click(function () {
            jQuery(form).trigger('reset');
        })
});

// Prevent empty search
let submitButton = jQuery('.buttonSubmitSearch');
let inputField = jQuery('.searchInput');
submitButton.click(function(e) {
    if(inputField.val().length<=0) {
        e.preventDefault();
    }
})


// Facebook page plugin load on hover START
let facebookPageLike = document.querySelector('.fb__page');
if (facebookPageLike !== null) {
    // facebookPageLike.innerHTML = '<img src="/wp-content/themes/wptheme/images/srpskainfo-fb-page-new.png?v=1" alt="SrpskaInfo Facebook Page">';
    facebookPageLike.innerHTML = '';
    let facebookPageLikeHTML = '<div class="fb-page" data-href="https://www.facebook.com/srpskainfocom/" data-lazy="true" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/srpskainfocom/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/srpskainfocom/">Srpskainfo.com</a></blockquote></div>';

    let fbRoot = document.createElement('div');
    fbRoot.id = "fb-root";
    let fbScript = document.createElement('script');
    fbScript.crossorigin = "anonymous";
    fbScript.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v7.0";
    fbScript.nonce = "ALST7EF6";

    let is_mobile = window["isMobile"];
    if (is_mobile === "0") {
        let hovered = false;
        facebookPageLike.addEventListener('mouseover', function () {
            if (!hovered) {
                facebookPageLike.innerHTML = facebookPageLikeHTML;
                document.body.prepend(fbScript);
                document.body.prepend(fbRoot);
                hovered = true;
            }
        });
    } else {
        // mobile version
        let loaded = false;
        jQuery(window).scroll(function () {
            // var hT = jQuery('.fb__page').offset().top,
            //     hH = jQuery('.fb__page').outerHeight(),
            //     wH = jQuery(window).height(),
            let wS = jQuery(this).scrollTop();
            // if (wS > (hT + hH - wH)) {
            if (wS > 500) {
                if (!loaded) {
                    facebookPageLike.innerHTML = facebookPageLikeHTML;
                    document.body.prepend(fbScript);
                    document.body.prepend(fbRoot);
                    loaded = true;
                }
            }
        });
    }
// Facebook page plugin load on hover END

}

//Gallery Tracking
jQuery(document).ready(function ($) {
    jQuery(window).on('hashchange', function () {
        window.dm.AjaxEvent("pageview");
        // gtag('send', 'pageview');
        gtag('event', 'page_view', {
            page_location: location.href.replace('#', ''),
            page_path: location.pathname + location.hash.replace('#', '')
        });
    });
});


// Fix youtube embeds for classic blocks (data-src is for lazyloaded things)
jQuery(".article__center iframe[data-src^='https://www.youtube.com']").wrap("<figure class='wp-block-embed-youtube'><div class='wp-block-embed__wrapper'></div></figure>");
jQuery(".article__center iframe[src^='https://www.youtube.com']").wrap("<figure class='wp-block-embed-youtube'><div class='wp-block-embed__wrapper'></div></figure>");


// Doesnt work reliably
// jQuery(document).ready(function () {
// // Hide empty banners inside article content on the single page
//     let targets = ['#Billboard', '#InText_1', '#InText_2', '#Billboard_UnderArticle']
//     targets.forEach(function (item) {
//         let jqObj = jQuery(item);
//         if (jqObj.attr('style') === 'display: none;'){
//            jqObj.parent().hide();
//         }
//     })
// })
