<?php
/* Template Name: Blic Posts Popular */

use GfWpPluginContainer\Wp\PostHelper;

function blic_new_posts() {
	$popularPosts = postHelper::getPopularPosts( 5 );

	$html = '<ul>';
    $params = '';
    $utmSource = '';
    $utmMedium = '';
    if (isset($_GET['utm_source']) && $_GET['utm_source'] !== ''){
        $utmSource = $_GET['utm_source'];
    }
    if (isset($_GET['utm_medium']) && $_GET['utm_medium'] !== ''){
        $utmMedium = $_GET['utm_medium'];
    }
    if ($utmSource !== '' && $utmMedium === '') {
        $params = '?utm_source='.$utmSource;
    }
    if ($utmMedium !== '' && $utmSource === '') {
        $params = '?utm_medium='.$utmMedium;
    }
    if ($utmMedium !== '' && $utmSource !== ''){
        $params = '?utm_source='.$utmSource.'&utm_medium='.$utmMedium;
    }
	foreach ( $popularPosts as $post ) {
		$html .= '<li>';
		$html .= '<div class="image">';
		$html .= '<a href="' . get_the_permalink($post->ID) . '" title="' . esc_attr($post->post_title) . '">';
		$img = get_the_post_thumbnail_url( $post->ID, 'medium' );
		$html .= '<img src="' . $img . '" alt="' . esc_attr($post->post_title) . '" title="' . esc_attr($post->post_title) .'">';
		$html .= '</a>';
		$html .= '</div>';
		$html .= '<div class="text">';
		$html .= '<a href="' . get_the_permalink($post->ID) . '" target="_blank" title="' . esc_attr($post->post_title) . '">' . get_the_title($post->ID) . '</a>';
		$html .= '</div>';
		$html .= '</li>';
	}

	$html .= '</ul>';

	return $html;
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<div class="latest-news">
    <a href="https://srpskainfo.com" target="_blank" title="<?= get_bloginfo('name') ?>">
        <div class="silogo"></div>
    </a>
	<?php echo blic_new_posts(); ?>
</div>
<style type="text/css">
    body {
        margin: 0;
    }

    .silogo {
        height: 28px;
        width: 120px;
        margin-top: 5px;
        margin-left: 13px;
        background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAAcCAYAAACqAXueAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTnU1rJkAAANAUlEQVRoQ+1aCXBV5RUOqQutttUZqxPAsD0SCBqEIElISIwECBgQWVxABETcwIUWGbAqrVioWKCiFQUVFUXBymAVVKQE0cG6oYLsArIokOS9+9b79nv6nXPv2++DhKajOPlnvrn3/uf8/73v/845/zl/ktHcmltzO3HLLCgoONO4/+k1e3b2+dY2HedYW1tWnY5wjLl5laZpqxzVw0zlTY42lpX12Z0qee1mzpyZ2adPnxnAsvLy8s7oasH9p9ry8/PPKS0tHYr5FuI6u6ioqIchOvVWm905q76N5SMgfDrCMWZCGASH7dXDTOVNDZCs1rdqfzOvHUi1gAhXSUlJkInG8xmyqKfQqqqqfoO53gS8gB/zBTDvGoj+J6PJqLs4t1VdG8tmWCZF0TaXbF0uI1vXHmTLuZSsF3eKydIBY06o16EL2Tobc+Z20/XN9JJg69iVbHkYg+8xG+O86Rbi5hg8PEXWYLTHt+V1178NV/5WUz2gvnVHX32WZQKvHQi9AER8DUIOA2NGjhz5C1nURjZ46i9B5jOYI4T51gPDgeuA/obKqbdkgm2XXk7qnMfI//Y7FFi3nvyvryLX7XfppMT90Jh+T3JPmUbeZ5eSOm8hKQW9E+VYNNekKeR7YRn531pDgfcw55tvk7poMTnHTCCr5ZIE/SjadSbn9TeR75XXMOZ9+R7v00vIMWAwWbNzononJBgGwf3O0eN0eZKB2PBu5/hbybcU3/bOe/rvfWcd+V56hVx33EO2SwoS9BnxBHMrKyvrBGIKmSSjq9EN48tB5kHgGIjONrqbpiUQDA/0LlpCmstF4SNHKLRjJ2k2G4Xr6sn73Itk7ZRIhlJYRoGaD0iz24nCYQofPkJK9yJdjrns5f0osGEjaVYbaaoXc35P4f0HKGy1EgUCFD5+nHwr3yDlssKEeRmuW+6g8KHDpHm9FNqzV+Ymf4BCO3fBMG6ORot0BNs65JFn1l8xxyEKbtsOPRhTnGEol5eSf/VbFK6tJQ3zho8dw7ftp/D3P+A9fvnmwEeb5TfEz5tMcFO03r173wWSXbiuwGODQjKiR0vo/7q6uvpXRpd5iydY6VFMmscDT1tL9j6VElIdQ4ZT8PMtQrzV0jW2QD1LKLjlK6JQiIKffSFe7KjGIhsL7xhwNYX37RN5YPMn4o1Kz1JSuvWSudUFC0lTFCHa+8yzCRHC1ulSjPlYxjJJ7En8PnX+QhB2hJw3nphgDusqvF1TVTFWz/0z0ZcXkyOqBNbXyPxabR25ZzxISnE5Kfm9SOlVSu5pf9QNCkYb/PgTeXdkbDLBWOjz4HlZZvtvv379zoGsBzAZ4GRsBjy+ON7bjUTtQRDsxXW60Z22Qa8DsACevgX624GteF6Cay/TbD6eYNe4ibIo7nunRReQwR5my8mPPjPRPoRuIffTz8neuyImA9gw2LOFXIQ9Npx4uQDzOwYMoeA3OxABHBIqIx7GCxrasUu8SbmkZ2wM5ErxFQmhNplg3mK8i58Xzw+DPM/MWRLuo3PgXp09Vww5hAjhuHZ0wm+NwF6Fb9u+U6IGG6PNiF7JBGNxZ2Fxv8E1x+hi0s9F33CQwPuzH3Dj2WFcfbi+gStnyJm8b+N5JsDJ1cNAjoHzIY96Mz/DOO7G9SigYPxunh/Ygft6wIn+V3BtbwzRWzzBjqqriXx+3aM4qYn7wfFwYlE43Go2BV47LFU+diLCby3I2w6PKEuRx8MxEnMdOy7RgMnhPgX7eujrreJ9sk/HhdZkxBNsv6K/RB/N56PgF1/qnp6077IxhtiosE04RowyJTcC+5UDSau3UvjgIbL3HSh9JgQ/hkU9CFK5TOJs+Gw8T0V/PXAA94+ChFEgZxjuRwPz0X8E2IYQe1kcwT7o1eK6x8D4SNIGvVaQcRLmxTwf4DoCyMFcWUB7yAbj+TWWA5tYxuOkJezB2LcCG/Q91bdsuYTS+B8swGKrf39SQisnQCmLj2fvE4tkH3PdeU+izARsSJxAsb590FC9n9/x6DwxttC3+8g9+fdIiGLbQzwiBKtzF4hBUQhhlaPKQMxlYhhOkKo5XRKBrAjlyfIEgHz/v9ZIqHbfd78Yy8kI5sUF9gIcQnskJ18ctkEIh2Q3rks5tONePBj68ziEM5g8qLdgOfQ4vDuBmoqKirboz5TJYq1FcXHxhZjjIYAjxMMI1/renEAwYO87iEK79xIFg0iu6kidNQf7Yyw8c+bpW/FPIk2TTDPSH5Vj/+TMO4zkzH7FgBR5CrCI6lOLZRFdd9wd7ectgTNvToA0N+cFa0gpQ8KT5HERgtkY+Ju4cTRQisoT9CJwTbxTJ2zq9BTvNgNXFDwvGzWH9wYQXA1CXLhOjHhgcmNdyPdg3OG8vLyzcB8heLKhEm3o/x363wdsuM83uk0b5m0JnX8Du0C4fE8KwbyATIwfJHIWywvne+11SZBYLgQi8+X91TlqXMJiiBx1s/+N1ZJ52/v0TZGbQX1sgUQE96R7E2UgwDXhdnjkZ5Ib8JzuP0yXJCqiEyGY91PP3PmS+TOB3udeSMwbDLhum2wYJ4zJxMOT4Z7+oE7w4/9oEMG4n4Q+FeiLR9OMGLrnwgg2AyH2NOimJRg63QAmdwEbg9GdtkHvRoANrBqPmakEG+BFtKPm9L20HB7kJi9qRSsy0QjBXPY4r7k2YYyMMwjmJMYxZESKPBn8Ht+rK3WD4T0xWQcGp/ToLZlw6PBhKWucN4yNyiMEu26dJAbhvOEmSc40r0+8LyHBYv3R4yUBU7GNWNsnylKA+QLvb4gRDP0GePAcQGFiRMGkcZiGzgaM0+BpbXGflmDI+2IuJux6zriN7rQN4b07dN3AjRJB0hEcAWe0gY0fUviHoxL2YgSr5Bw6MkU/QjAviuzRSSE1GZyEBb/eRuGjx1Cb9jHVEYAo5/jbJIFS5z0up0/cH59kiR7yCJbrJZJb6un4b7BXXiUGENr7rRzSRPrN4Bw1Fpu72lgPng98F3k2a/EEFxYWdsH9iTx4IGScIY9oCMHQywc8eL9+snYygmN7qiKZZGMIZi92IktO1okCC+/50yPivd6lLwk5pnoGuETSHE7yPvm0HGRwXwrBrNe9GLX7F/INwa3bEvZjLuF8q9+SZMy37NUUD4+AtyTZorg1woOZYPTtR+abKwomrTEEo78E8IDoSXjHSc+6oVPJcwEjxSCiBGM/4j3Vt3wlKSVX6gsIL+GFC+3cLXUpW3yDCQZpXEaFDhyUUofPk2UxEfb40IHJYnLZy4JfbSV7ef/oHApKGS53uNyydS2Q7+BaXAWxvFd7HkJta+yfZgQzHFddoxOEZNH7NMq+zrGDFD50Ce3br5P84stkR/LGv0vOy1GqcekW3PKl7OV8wkXBUKM8GH1NRjCHcPRzPf0pdC8yutM1PjThv0Qdwvt74rlFjGAQyufJvIC84L4XXiYvstjQ7j1S07rvmaqT00CCNY+KBOUBOarkfZPPePk0TP3b45Idh1DSaCiNOON1DLtB5o7MwWfHso86HORft16OSQObPtLrW+jHl2/pCObowIbFdaym2JE1zwBBxjvYmDGOT+D497Lx8jf7lq+gQM0mqZE1JJdcIrmx93Mk+rE8mOtqyGYDXFY9w2MNUUJj78b466BXBzxRWVn5WxHEh2g+qvM8PFu8ji2fLTh04Dty33ufbuHQaTDBqKX5oIDDIx8xypxYTCZJFnXXblIXPCHeE0+uzIHEyzn2FvKvXSclEn8HH3r4315L9v6N+2OD589/kRqbDcZRNSQmwxwcRTgR42+R38sNhhnYuEl+M0cNjmpRgn8ED+YGWQfINgIq8Dze1Q7d0QwdfVnQ4Rr4KO53w+u7o1uXp+zB+BFcXvDC8wJIqZFMAGrhdH++ixDMNbS91CiTInPC8xyDhurJGs/LITtpfBQggInmc2h7WaW+PfB5clLSZuvSXeY0K4lEDoO096smx0DomB2ZGt+m5CM09x0k8/Hvs7bVjUjO4wcMIaWgRN79YxCMxgcZHKo3Qy8E/ID7V+HRj+D6FK770OfH1YbvqIJ+LBk7WZLVWEQJro0j+GeEZIKxqA9gkf+DhbXws0HWZhDcURRMmlH7rgAOFRUV8T8NTMEc3wLjDBXTxn9MgM5U4EOM4eNM/jv0fjx/jO+Yh+vFhmqsNRPcOCQTzIuKRc4HwS3NntO0zIqKilzo9GQ9GEMrjCmAl15oyE/UMjGG/5jRHuN4fGfeb9OdmmUcaNv2vLrWlunAkqaANbfbs/61764O22zv2iuqlprpnM4AwYtsWR1KjeX76TdCfCe4PuXlndUU2GOxnE01NS2RrrTcgwzQTOd0x8qMjFP615zm1tz+P+3zjIIz2fOa8fMBIrOeSR+96KJzaltbpiDRerEZPx8cz+5QIiTXZGScUdeqXW59a0uRo3WnQr5G0Px8+j47s3IuoIyMFv8Fc0xMv2uiTqcAAAAASUVORK5CYII=')

    }

    .latest-news {
        max-width: 300px;
        max-height: 300px;
    }


    .latest-news a:hover {
        color: #ea2e2e;
    }

    .latest-news li {
        border-bottom: 1px solid #eee;
        list-style: none;
        overflow: hidden;

    }

    .latest-news ul {
        padding: 0;
        margin: 0;
    }


    .image {
        float: left;
        width: 105px;
        margin: 14px 0 10px 14px;
    }

    .latest-news .text a {
        font-family: "Roboto", sans-serif;
        color: #424242;
        font-weight: 700;
        font-size: 14px;
        line-height: 20px;
        text-decoration: none;
        float: right;
        width: calc(100% - 147px);
        margin-top: 8px;
        margin: 10px 14px 8px 14px;
        max-height: 78px !important;
        overflow: hidden;
    }


    .image img {
        width: 105px;
        height: 70px;
        object-fit: cover;
    }

</style>
</html>