<?php
/*
 * Use template name as directory name in templates folder and
 * when naming css and js file for that template
 *   inside css and js directories
 *   directory structure
 *   dir template >
 *   dir view > template.php
 *   dir css > template.css
 *   dir js > template.js
 *
 * 'Example shortcode' =>
 *   [
 *       'name' => 'my_shortcode',
 *       'template' => 'myTemplateName'
 *   ],
 *
 */

return
    [
        [
            'name' => 'header',
            'template' => 'header'
        ],
        [
            'name' => 'footer',
            'template' => 'footer'
        ],
        [
            'name' => 'pslide',
            'template' => 'pslide'
        ],
        [
            'name' => 'fbpage',
            'template' => 'fbpage'
        ],
        [
            'name' => 'blicNaslovna',
            'template' => 'blicNaslovna'
        ],
        [
            'name' => 'fbvideo',
            'template' => 'fbVideo'
        ],
        [
            'name' => 'fb',
            'template' => 'fbVideo'
        ],
        [
            'name' => 'caption',
            'template' => 'caption'
        ]
    ];
