<?php
/**
 * @see \gfTheme\GfShopTheme::registerSidebar()
 */
/*
 * Use this format when adding new sidebars to array
 'sidebarName' => [
	'name'          => '' ,
    'id'            => '' ,
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="rounded">',
    'after_title'   => '</h2>'
 ]
 */
return  [
    'Top Bar Right' => [
        'name'          => 'Top Bar Right' ,
        'id'            => 'top_bar_right' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Footer Bar Right' => [
        'name'          => 'Footer Bar Right' ,
        'id'            => 'footer_bar_right' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Blic Naslovna Sidebar' => [
        'name'          => 'Blic Naslovna Sidebar' ,
        'id'            => 'blic_naslovna_sidebar' ,
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Category Page Top Banner' => [
        'name'          => 'Category Page Top Banner' ,
        'id'            => 'category_page_top_banner' ,
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Category Page Sidebar' => [
        'name'          => 'Category Page Sidebar' ,
        'id'            => 'category_page_sidebar' ,
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Category Page Sidebar Mobile' => [
        'name'          => 'Category Page Sidebar Mobile' ,
        'id'            => 'category_page_sidebar_mobile' ,
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Single Page Sidebar Desk' => [
        'name'          => 'Single Page Sidebar Desk' ,
        'id'            => 'single_page_sidebar_desk' ,
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Single Page Sidebar Mobile' => [
        'name'          => 'Single Page Sidebar Mobile' ,
        'id'            => 'single_page_sidebar_mobile' ,
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Single Page Sidebar Amp' => [
	    'name'          => 'Single Page Sidebar Amp' ,
	    'id'            => 'single_page_sidebar_amp' ,
	    'before_widget' => '',
	    'after_widget'  => '',
	    'before_title'  => '<h2>',
	    'after_title'   => '</h2>'
    ],

    'Single Post Top Banner' => [
        'name'          => 'Single Post Top Banner' ,
        'id'            => 'single_post_top_banner' ,
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Single Post Bottom' => [
        'name'          => 'Single Post Bottom' ,
        'id'            => 'single_post_bottom' ,
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Single Post Bottom Mobile' => [
        'name'          => 'Single Post Bottom Mobile' ,
        'id'            => 'single_post_bottom_mobile' ,
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Single Post Bottom Banner' => [
        'name'          => 'Single Post Bottom Banner' ,
        'id'            => 'single_post_bottom_banner' ,
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Single Post Bottom Banner Mobile' => [
        'name'          => 'Single Post Bottom Banner Mobile' ,
        'id'            => 'single_post_bottom_banner_mobile' ,
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Post Social Sidebar' => [
        'name'          => 'Post Social Sidebar' ,
        'id'            => 'post_social_sidebar' ,
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Category InFeed banner 1' => [
        'name'          => 'Category InFeed banner 1' ,
        'id'            => 'category_feed_baner_1' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Category InFeed banner 2' => [
        'name'          => 'Category InFeed banner 2' ,
        'id'            => 'category_feed_baner_2' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Category InFeed banner 3' => [
        'name'          => 'Category InFeed banner 3' ,
        'id'            => 'category_feed_baner_3' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Category InFeed banner mobile 1' => [
        'name'          => 'Category InFeed banner mobile 1' ,
        'id'            => 'category_feed_baner_mobile_1' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Category InFeed banner mobile 2' => [
        'name'          => 'Category InFeed banner mobile 2' ,
        'id'            => 'category_feed_baner_mobile_2' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Category InFeed banner mobile 3' => [
        'name'          => 'Category InFeed banner mobile 3' ,
        'id'            => 'category_feed_baner_mobile_3' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Category InFeed banner mobile 4' => [
        'name'          => 'Category InFeed banner mobile 4' ,
        'id'            => 'category_feed_baner_mobile_4' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Single InText banner 1' => [
        'name'          => 'Single InText banner 1' ,
        'id'            => 'single_intext_feed_baner_1' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Single InText banner 2' => [
        'name'          => 'Single InText banner 2' ,
        'id'            => 'single_intext_feed_baner_2' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Single InText banner 3' => [
        'name'          => 'Single InText banner 3' ,
        'id'            => 'single_intext_feed_baner_3' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Single InText banner mobile 1' => [
        'name'          => 'Single InText banner mobile 1' ,
        'id'            => 'single_intext_feed_baner_mobile_1' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Single InText banner mobile 2' => [
        'name'          => 'Single InText banner mobile 2' ,
        'id'            => 'single_intext_feed_baner_mobile_2' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ],
    'Single InText banner mobile 3' => [
        'name'          => 'Single InText banner mobile 3' ,
        'id'            => 'single_intext_feed_baner_mobile_3' ,
        'before_widget' => '<li>',
        'after_widget'  => '</li>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ]
];

