<?php
return [
    'languageTextDomain' => ['gfShopTheme' => PARENT_THEME_DIR_URI . '/languages'],
    'supportedFeatures' => [
        'title-tag',
        'custom-logo',
        'post-thumbnails',
        'customize-selective-refresh-widgets',
        'html5' => [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption'
        ],
        'woocommerce',
        'menus',
        'post_format' => [
            'audio',
            'video',
            'gallery']
    ],
    'styles' => [
        'frontendStyleMain' => PARENT_THEME_DIR_URI . '/style.css',
        'googleFonts' => 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap'
    ],
    'scripts' => [
        'fontAwesome' => 'https://kit.fontawesome.com/adf4c66ded.js',
        'mainJs' => PARENT_THEME_DIR_URI . '/js/main.js'
    ],
    'navMenus' => [
        'Main Navigation',
        'Footer Menu Bottom',
        'Footer Menu Center',
        'Footer Menu Left',
        'Footer Menu Right',
	    'Mobile Menu Bottom'
    ],
    'customize' => [
        'logos' => [
            'Side Logo' => [
                'id' => 'gf_side_logo',
                'label' => 'Side Logo',
                'section' =>'title_tagline',
                'settings' => 'gf_side_logo',
            ],
            'Logo' => [
                'id' => 'gf_logo',
                'label' => 'Logo',
                'section' =>'title_tagline',
                'settings' => 'gf_logo',
        ]]
    ],
	'pages' => [
		'Es Archive' => 'customCategory.php'
	]
];