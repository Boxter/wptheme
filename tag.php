<?php

use \GfWpPluginContainer\Indexer\Repository\Article as ArticleRepo;

get_header();
$paged = (get_query_var('paged') > 0) ? get_query_var('paged') : 1;
//$slug = explode('/', $_GET['q'])[2];
//if (strlen($slug) === 0) {
$slug = explode('/', $_SERVER['REQUEST_URI'])[2];
//}
$tag = get_term_by('slug', $slug, 'post_tag');
$catName = $tag->name;
//$dotMetricsId = getDotMetricsId('tag');
$dotMetricsId = 4595;
//Needed for infinite scroll
$ajaxAction = 'tag';
$ajaxTermValue = $slug;

//$searchFunctions = new \GfWpPluginContainer\Elastic\Functions($wpdb);
if (USE_ELASTIC !== true) {
    $sortedItems = ArticleRepo::getItemsFromWp([
        'category_name' => $catName,
        'paged'=> $paged,
        'posts_per_page' => PER_PAGE
    ]);
} else {
    $sortedItems = ArticleRepo::getItemsFromElasticBy('tag', $tag, $paged, PER_PAGE);
}

if ($isApp) {
    include "templates/archive/archiveMobileApp.php";
    wp_footer();
} else {
    include "templates/archive/archiveDesktop.php";
	get_footer();
}
