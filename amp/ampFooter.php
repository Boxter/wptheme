</main>
<footer style="font-size:14px; text-align:left;" class="footer">
	<div class="container">

		<div class="footer__top">
			<div class="footer__menu">
				<h3>Vesti</h3>
				<ul>
					<?php
					$menu = wp_get_nav_menu_items('Footer Menu Left');
					foreach ($menu as $menuItem) :?>
						<li><a title="<?= $menuItem->title ?>" style="color:#212121;" href="<?= $menuItem->url ?>"><?= $menuItem->title ?></a></li>
					<?php endforeach; ?>
				</ul>
				<ul>
					<?php
					$menu = wp_get_nav_menu_items('Footer Menu Center');
					foreach ($menu as $menuItem) :?>
						<li><a title="<?= $menuItem->title ?>" style="color:#212121;" href="<?= $menuItem->url ?>"><?= $menuItem->title ?></a></li>
					<?php endforeach; ?>
				</ul>
				<ul>
					<?php
					$menu = wp_get_nav_menu_items('Footer Menu Right');
					foreach ($menu as $menuItem) :?>
						<li><a title="<?= $menuItem->title ?>" style="color:#212121;" href="<?= $menuItem->url ?>"><?= $menuItem->title ?></a></li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="footer__social">
				<h3>Društvene mreže</h3>
				<ul>
					<?php dynamic_sidebar("footer_bar_right"); ?>
				</ul>
			</div>
		</div>

		<div class="footer__bottom">
			<nav>
				<ul>
					<?php
					$menu = wp_get_nav_menu_items('Footer Menu Bottom');
					foreach ($menu as $menuItem) :?>
						<li><a title="<?= $menuItem->title ?>" style="color:#212121;" href="<?= $menuItem->url ?>"><?= $menuItem->title ?></a></li>
					<?php endforeach; ?>
				</ul>
			</nav>
			<span style="padding-top:0;">© Sprska info EUROBLIC <?php echo date("Y");?></span>
		</div>
	</div>
    <amp-analytics config="https://script.dotmetrics.net/AmpConfig.json?id=<?=$dotMetricsId?>"> </amp-analytics>
</footer>

