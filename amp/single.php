<?php

use Carbon\Carbon;
use GfWpPluginContainer\Wp\PostHelper;

$catSlug = get_the_category()[0]->slug;
$dotMetricsId = getDotMetricsId($catSlug);
?>

<!doctype html>
<html amp <?php echo AMP_HTML_Utils::build_attributes_string($this->get('html_tag_attributes')); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <?php do_action('amp_post_template_head', $this); ?>
    <style amp-custom>
        <?php $this->load_parts( array( 'style' ) ); ?>
        <?php do_action( 'amp_post_template_css', $this ); ?>
    </style>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700&#038;ver=5.4.2">
    <script async custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"></script>
    <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
    <amp-analytics type="gtag" data-credentials="include">
        <script type="application/json">
            {
                "vars" : {
                    "gtag_id": "UA-83076923-7",
                    "config" : {
                        "UA-83076923-7": { "groups": "default" }
                    }
                }
            }
        </script>
    </amp-analytics>
</head>

<body class="<?php echo esc_attr($this->get('body_class')); ?>">

<?php include 'ampHeader.php'; ?>
<article class="article">
    <?php while (have_posts()) : the_post();
        $content = get_the_content();
        $post = get_queried_object();
        $multipleAuthors = new GfWpPluginContainer\Wp\MultipleAuthors\MultipleAuthors();
        $postOwners = $multipleAuthors->getOwnersForPost(get_the_ID());
        $categories = get_the_category();
        $catName = $categories[0]->name;
        $catLink = str_replace('/category', '', get_category_link($categories[0]->term_id));
        $imageUrl = esc_url(wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'list-big'));
        $blocks = count(parse_blocks($post->post_content)) > 0 ? parse_blocks($post->post_content) : null;
        $lead = '';
        // If there is a heading block, get the first one
        if (isset($blocks) && $blocks[0]['blockName'] == 'core/heading') {
            $lead = $blocks[0]['innerHTML'];
            $lead = str_replace(['<h2>', '</h2>'], '', $lead);
            unset($blocks[0]);
        }
        $renderedBlocks = [];

        foreach ($blocks as $block){


            if ($block['blockName'] === 'core/image') {
                $imageData = wp_get_attachment_metadata($block['attrs']['id']);
                $imageCaption = wp_get_attachment_caption($block['attrs']['id']);
                $imageLegenda = get_post_meta($block['attrs']['id'], 'legenda', true);
                $imageSize = 'list-big';
                $imageWidth = '427';
                $imageHeight = '285';
                if (isset($block['attrs']['sizeSlug']) &&$block['attrs']['sizeSlug'] === 'full') {
                    $imageSize = 'landsape-m';
                    $imageWidth = '400';
                    $imageHeight = '200';
                } else if (isset($imageData['width'],$imageData['height']) && $imageData['width'] < $imageData['height']) {
                    $imageSize = 'portrait-m';
                    $imageWidth = '400';
                    $imageHeight = '800';
                }
                $imageUrl = wp_get_attachment_image_url($block['attrs']['id'], $imageSize);
                $block['innerContent'][0] = '<figure class="captionImageWrapper"><img src="'.$imageUrl.'" alt="'.esc_attr($imageCaption).'"
                    width="'.$imageWidth.'" height="'.$imageHeight.'" />
                    <figcaption class="captionImageCaption">'.$imageCaption .'</figcaption>
                    </figure>
                    <span class="keySingle">' . $imageLegenda . '</span>';
            }

            $block = apply_filters('the_content', render_block($block));
            $block = str_replace(['<br/>', '<br />', '<br>'], '', $block);

            // detect lead type ( check for h2, h3, h4 ) if not found then extract first <p> tag
            if (!strlen($lead)) {
                $start = mb_strpos($block, '<p>');
                $end = mb_strpos($block, '</p>', $start);
                $lead = mb_substr($block, $start, $end - $start + 4);
            }

            $renderedBlocks[] = str_replace($lead, '', $block);
        }

        //Featured Image
        $featuredImageId = get_post_thumbnail_id($post->ID);
        $imageUrl = esc_url(wp_get_attachment_image_url($featuredImageId, 'list-big'));
        $imageWidth = 427;
        $imageHeight = 285;
        $imageLegenda = get_post_meta($featuredImageId, 'legenda', true);
        // Post dates
        $publishedDate = new Carbon($post->post_date);
        $updatedDate = new Carbon($post->post_modified);
        ?>
        <header>
            <div>
                <a title="<?= $catName ?>" class="categoryPageNameLink" href="<?= $catLink ?>"><?= $catName ?></a>
                <h1><?= the_title(); ?></h1>
                <p><?= strip_tags($lead, '<strong><h2><h3><h4>') ?></p>
            </div>
            <figure class="captionImageWrapper">
                <amp-img
                        alt="<?= the_title() ?>"
                        src="<?= $imageUrl ?>"
                        width="427"
                        height="285"
                        layout="responsive"
                >
                </amp-img>
                <figcaption class="captionImageCaption captionImageFeatured"><?= get_the_post_thumbnail_caption($post->ID) ?></figcaption>
            </figure>
            <div class="keyFeatured">
                <span><?= $imageLegenda ?></span>
            </div>
        </header>
        <aside class="article__aside-top">
            <div class="author">
                <?php if (count($postOwners) > 1 && count($postOwners) !== 0): ?>
                    <span>Autori:</span>
                <?php else:?>
                    <span>Autor:</span>
                <?php endif;
                $i = 1;
                $comma = ',';
                $count = count($postOwners);
                /** @var \GfWpPluginContainer\Wp\MultipleAuthors\Model\GfPostOwner $postOwner */
                foreach($postOwners as $postOwner):
                    if ($i === count($postOwners)) {
                        $comma = '';
                    }
                    $i++;
                    $ownerLink = get_author_posts_url($postOwner->getAuthorId());
                    $ownerDisplayName = $postOwner->getAuthorDisplayName();
                    ?>
                    <a href="<?=$ownerLink?>" title="<?=$ownerDisplayName?>"><?=$ownerDisplayName?></a><?=$comma?>
                <?php endforeach; ?>
            </div>
            <div class="time-date">
                <div class="date">
                    <amp-date-display datetime="<?= $publishedDate->toIso8601ZuluString() ?>" width="120" height="10"
                                      layout="responsive" display-in="UTC">
                        <template type="amp-mustache">{{day}}.{{monthName}}.{{year}}</template>
                    </amp-date-display>
                </div>
                <div class="time">
                    <i class="fa fa-circle"></i>
                    <amp-date-display datetime="<?= $publishedDate->toIso8601ZuluString() ?>" width="120"
                                      height="10" layout="responsive" display-in="UTC">
                        <template type="amp-mustache">{{hourTwoDigit}}:{{minuteTwoDigit}}</template>
                    </amp-date-display>
                    <?php
                    if ($publishedDate->format('Y/m/d/h:i') < $updatedDate->format('Y/m/d/h:i')) :?>
                        <i class="fa fa-arrow-right"></i>
                        <amp-date-display datetime="<?= $updatedDate->toIso8601ZuluString() ?>" width="120" height="10"
                                          layout="responsive" display-in="UTC">
                            <template type="amp-mustache">{{hourTwoDigit}}:{{minuteTwoDigit}}</template>
                        </amp-date-display>
                    <?php endif ?>
                </div>
            </div>
            <div class="social">
                <amp-social-share type="facebook" data-param-app_id="206933876746107" width="30"
                                  height="30"></amp-social-share>
                <amp-social-share type="twitter" width="30" height="30"></amp-social-share>
                <amp-social-share type="whatsapp" width="30" height="30"></amp-social-share>
            </div>
        </aside>
        <div class="article__content">
            <?php foreach ($renderedBlocks as $block) {
                echo $block;
            } ?>
            <div class="article__tags">
                <?= the_tags('', '', '') ?>
            </div>
            <div class="banner banner__top">
                <amp-ad layout="fluid"
                        height="fluid"
                        type="doubleclick"
                        data-slot="/22022651321/Srpskainfo.com/AMP_1"
                        data-multi-size="300x250,1x1">
                    <div overflow></div>
                </amp-ad>
            </div>
        </div>
    <?php endwhile; ?>
</article>

<div class="article__fb">
    <p>Možete da nas pratite i na&nbsp;Facebook stranici:</p>
    <amp-facebook-page
            width="340"
            height="130"
            layout="fixed"
            data-hide-cover="false"
            data-href="https://www.facebook.com/srpskainfocom/"
    >
    </amp-facebook-page>
</div>
<div class="banner banner__top">
    <amp-ad layout="fluid"
            height="fluid"
            type="doubleclick"
            data-slot="/22022651321/Srpskainfo.com/AMP_2"
            data-multi-size="300x250,1x1">
        <div overflow></div>
    </amp-ad>
</div>

<div class="article__comments">
    <div class="container">
        <amp-facebook-comments
                width="486"
                height="657"
                layout="responsive"
                data-numposts="5"
                data-href="<?= get_permalink(get_the_ID()) ?>">
        </amp-facebook-comments>
    </div>
</div>

<?php dynamic_sidebar('single_page_sidebar_amp'); ?>


<?php include 'ampFooter.php'; ?>

<?php do_action('amp_post_template_footer', $this); ?>
</body>
</html>