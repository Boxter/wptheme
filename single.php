<?php
// Check if request is coming from an APP and set route param in var
global $isApp;
get_header();
dynamic_sidebar('single_post_top_banner');
if ($isApp) {
    include 'templates/single/singleMobileApp.php';
} else {
    include 'templates/single/singleDesktop.php';
}
get_footer();
