* {
margin: 0;
padding: 0;
box-sizing: border-box
}

body {
overflow-x: hidden
}

a {
text-decoration: none
}

ul {
list-style-type: none
}

figure {
margin: 0
}

.clear {
clear: both
}

fieldset {
border: none;
padding: 0;
margin: 0
}

textarea:focus, input:focus {
outline: none
}

input, select, textarea {
border: 0
}

img {
width: 100%;
height: auto;
display: block
}

*:focus {
outline: 0
}

body {
font-family: \'Open Sans\', sans-serif;
font-weight: 400;
color: #231f20
}

a {
color: #212121
}

a:hover {
color: #ea1c24
}

h1, h2, h3, h4, h5, h6 {
font-weight: normal
}

h1 span, h2 span, h3 span, h4 span, h5 span, h6 span {
font-weight: bold
}

h1 {
font-size: 2em
}

h2 {
font-size: 1.5em
}

h3 {
font-size: 1.17em
}

h4 {
font-size: 1.12em
}

h5 {
font-size: .83em
}

h6 {
font-size: .75em
}

.header {
background: #FFFFFF;
border-bottom: 1px solid #ebebeb;
z-index: 2
}

.header .header__top {
padding: 10px 20px;
display: -webkit-box;
display: -webkit-flex;
display: -moz-flex;
display: -ms-flexbox;
display: flex;
align-items: center;
justify-content: space-between
}

.header .header__top > * {
flex: 1
}

.header .menu__button {
border: 0;
background: none;
font-size: 16px
}

.header .menu__button i:last-child {
background: #fff;
position: relative;
top: 8px;
left: -10px;
border-radius: 50%;
padding: 1px;
width: 17px;
height: 17px;
transform: rotate(90deg)
}

.header .logo a {
width: 180px;
margin: 0 auto;
display: block
}

.header .logo a span {
background: url("https://srpskainfo.com/wp-content/themes/srpskainfo/logo-site.svg");
background-size: 180px 33px;
width: 180px;
height: 33px;
display: block
}

.aside-menu {
background: #1a1a1a
}

.aside-menu nav {
padding: 20px
}

.aside-menu ul {
padding-bottom: 35px
}

.aside-menu li {
padding: 10px 0
}

.aside-menu li a {
color: #ffffff;
font-weight: bold;
font-size: 22px;
line-height: 1em
}

.aside-menu li.active a {
color: #ea1c24
}

.aside-menu img {
width: auto
}

.aside-menu__close {
color: #fff;
padding: 10px 0 10px 20px;
width: 100%;
font-size: 26px;
border: 0;
background: none;
text-align: left
}

.aside-menu__search {
margin: 0 20px 10px;
z-index: 1;
background: #1a1a1a;
border-bottom: 1px solid #595959
}

.aside-menu__search input[type=search] {
border: 0;
background: transparent;
font-size: 22px;
width: calc(100% - 47px);
font-weight: bold;
color: #595959
}

.aside-menu__search button {
color: #ffffff;
border: 0;
background: transparent;
font-size: 22px;
cursor: pointer;
padding: 10px
}

::-webkit-search-cancel-button {
-webkit-appearance: none
}

.article {
display: -webkit-box;
display: -webkit-flex;
display: -moz-flex;
display: -ms-flexbox;
display: flex;
flex-wrap: wrap;
justify-content: space-between;
padding: 20px
}

.article header h2 {
text-transform: uppercase;
font-size: 14px;
font-weight: bold;
letter-spacing: 1px
}

.article header h2 a {
color: #ea1c24
}

.article header h1 {
font-weight: bold;
font-size: 26px;
line-height: 1.15em;
margin: 10px 0 20px
}

.article header p {
font-size: 18px;
line-height: 1.4em;
margin-bottom: 20px
}

.article .author {
font-size: 14px;
margin-bottom: 2px
}

.article .author a {
font-weight: bold
}

.article .time-date {
display: -webkit-box;
display: -webkit-flex;
display: -moz-flex;
display: -ms-flexbox;
display: flex;
font-size: 14px
}

.article .date {
position: relative;
top: -1px
}

.article .time {
display: -webkit-box;
display: -webkit-flex;
display: -moz-flex;
display: -ms-flexbox;
display: flex;
align-items: center
}

.article .time i {
color: #595959;
font-size: 8px;
margin: 0 5px;
position: relative;
top: 2px
}

.article .time i.fa-circle {
font-size: 3px
}

.article figcaption {
text-align: right;
font-size: 12px;
padding: 5px 0;
color: #595959;
border-bottom: 1px solid #e6e6e4;
-o-text-overflow: ellipsis;
text-overflow: ellipsis;
display: -webkit-box;
-webkit-box-orient: vertical;
-webkit-line-clamp: 1;
max-height: 24px;
overflow: hidden
}

.article__content figure {
margin: 20px 0
}

.article__content figcaption {
font-size: 12px;
padding: 5px 0;
color: #595959
}

.article__content p {
font-size: 18px;
line-height: 1.4em;
margin-bottom: 16px
}

.article__content p a {
color: #ea1c24;
text-decoration: underline
}

.article__content > p:first-child::first-letter {
font-size: 3rem;
line-height: 1em;
margin: 0 10px 0 0;
font-weight: bold;
letter-spacing: 0;
color: #1a1a1a;
float: left
}

.article__content blockquote {
width: 100%;
margin-top: 20px;
float: left;
clear: left;
padding-top: 16px;
position: relative
}

.article__content blockquote:before {
content: "";
position: absolute;
top: 0;
left: 0;
background: #ea1c24;
width: 120px;
height: 8px
}

.article__content blockquote p {
font-size: 28px;
font-weight: bold;
line-height: 1.4em;
letter-spacing: -2px
}

.article__aside-top {
padding: 20px 0;
width: 100%
}

.article__aside-bottom {
padding: 20px
}

.social {
margin-top: 20px
}

.news {
display: -webkit-box;
display: -webkit-flex;
display: -moz-flex;
display: -ms-flexbox;
display: flex;
justify-content: space-between;
flex-wrap: wrap
}

.news__item {
margin-bottom: 20px;
display: -webkit-box;
display: -webkit-flex;
display: -moz-flex;
display: -ms-flexbox;
display: flex;
justify-content: space-between;
padding-bottom: 20px;
border-bottom: 1px solid #ebebeb;
width: 100%
}

.news__item .news__category {
display: block;
margin-top: -8px
}

.news__item .news__category a {
color: #ea1c24;
font-size: 12px;
letter-spacing: 1px;
text-transform: uppercase
}

.news__item:last-child {
margin-bottom: 0;
padding-bottom: 0;
border-bottom: 0
}

.news__item > a {
width: 120px
}

.news__item > div {
margin-top: 0;
width: calc(100% - 120px - 20px)
}

.news__item h1, .news__item h2, .news__item h3 {
font-size: 16px;
font-weight: normal
}

.box {
margin: 20px
}

.box__title {
font-size: 16px;
font-weight: bold;
text-transform: uppercase;
color: #ea1c24;
border-bottom: 8px solid #ea1c24;
display: inline-block;
margin-bottom: 20px
}

.box__title a {
color: #ea1c24
}

.footer {
font-size: 14px;
margin: 40px 0
}

.footer .container {
border-top: 2px solid #ea1c24;
padding: 20px
}

.footer__top {
display: -webkit-box;
display: -webkit-flex;
display: -moz-flex;
display: -ms-flexbox;
display: flex;
flex-direction: column;
justify-content: space-between;
padding-bottom: 40px
}

.footer__top h3 {
font-weight: bold;
margin-bottom: 20px
}

.footer__top li {
margin-bottom: 10px
}

.footer__menu {
display: -webkit-box;
display: -webkit-flex;
display: -moz-flex;
display: -ms-flexbox;
display: flex;
flex-wrap: wrap;
flex-direction: column
}

.footer__menu h3 {
width: 100%
}

.footer__menu ul {
flex: 1
}

.footer__social {
margin-top: 20px
}

.footer__social span {
width: 18px;
display: inline-block
}

.footer__bottom {
display: -webkit-box;
display: -webkit-flex;
display: -moz-flex;
display: -ms-flexbox;
display: flex;
justify-content: space-between;
flex-direction: column
}

.footer__bottom span {
padding-top: 20px
}

.footer__bottom nav ul {
display: -webkit-box;
display: -webkit-flex;
display: -moz-flex;
display: -ms-flexbox;
display: flex
}

.footer__bottom nav ul li a:after {
content: "|";
display: inline-block;
position: relative;
top: 0;
right: 0;
padding-left: 12px;
padding-right: 12px;
box-sizing: border-box
}

.footer__bottom nav ul li:last-child a:after {
content: none
}

amp-social-share[type=\'twitter\'] {
background: white;
background-image: url("'.$path.'assets/twitter.svg");
background-repeat: no-repeat;
}

amp-social-share[type=\'facebook\'] {
background: white;
background-image: url("'.$path.'assets/facebook-f.svg");
background-repeat: no-repeat;
}

amp-social-share[type=\'whatsapp\'] {
background: white;
background-image: url("'.$path.'assets/whatsapp.svg");
background-repeat: no-repeat;
}

