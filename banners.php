<?php

$appSinglePost = null;

function getAdsScript( $url ) {
    global $appSinglePost;
    if ( ! $url ) {
        $url = '//' . $_SERVER['REQUEST_URI'];
    }
    // Check if the request is coming from the mobile app
    $isMobileApp =  $_SERVER['REQUEST_URI'] === '/mobile-home/' || strpos( $_SERVER['REQUEST_URI'], '/app/' ) !== false;
    $isOtherAppPage = '';
    // If is mobile app, and on tag/search/author page set $isOtherAppPage as $_GET['type](will be tag/search/author)
    if ($isMobileApp && (isset($_GET['type']) && ($_GET['type'] === 'tag' || $_GET['type'] === 'search' || $_GET['type'] === 'author'))) {
        $isOtherAppPage = $_GET['type'] ?: '';
    }
    /*  If is mobile app, set the path to $_GET['url'] for category pages, $_GET['type'] for author/tag/search pages,
     and 'mobile-home' if on home, else (if not mobile app) set $path depending on the url
    */
    if ($isMobileApp) {
        if (strlen($isOtherAppPage) > 0) {
            $path = $_GET['type'];
        } elseif(explode('/', $url)[3] === 'mobile-home') {
            $path = 'mobile-home';
        } else {
            $path = $_GET['url'];
        }
    } else {
        $path = explode('/', $url)[3];
    }

    $singleScript = 'googletag.cmd.push(function() {
var mapBillboard = googletag.sizeMapping()
       .addSize([1024, 0], [[970,90],[970,250],\'fluid\', [970,400],[728, 90],[990,250]])
       .addSize([768, 0],  [[728, 90],\'fluid\', [468, 60]])
       .addSize([0, 0],    [[320, 100],[320, 50], [300, 50],[300, 100]])
       .build();
   var mapBoxes = googletag.sizeMapping()
       .addSize([1024, 0], [[300, 600],\'fluid\',[300, 250],[336, 280], [1,1]])
       .addSize([768, 0],  [[160, 600],\'fluid\',[300, 250],[336, 280], [1,1]])
       .addSize([0, 0],    [[320, 100],\'fluid\', [320, 50], [300, 50], [200, 200], [250, 250], [300, 100], [300, 250], [336, 280], [1,1]])
       .build();
   var mapInText = googletag.sizeMapping()
   .addSize([1024, 0], [\'fluid\',[300, 250],[336, 280], [1,1]])
   .addSize([768, 0],  [\'fluid\',[300, 250],[336, 280], [1,1]])
   .addSize([0, 0],    [[320, 100],\'fluid\', [320, 50], [300, 50], [200, 200], [250, 250], [300, 100], [300, 250], [336, 280], [1,1]])
   .build();
   var mapUnderText = googletag.sizeMapping()
   .addSize([1024, 0], [\'fluid\', [1, 1], [300, 250], [728, 90], [336, 280]])
   .addSize([768, 0],  [\'fluid\', [1, 1], [300, 250], [728, 90], [336, 280]])
   .addSize([0, 0],    [[1,1]])
   .build();
   gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/Billboard\', [[728, 90], \'fluid\', [300, 100], [990,250], [970, 90], [468, 60], [970, 400], [970, 250], [320, 50], [300, 50], [320, 100]], \'Billboard\').defineSizeMapping(mapBillboard).addService(googletag.pubads()));
   gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P1\',[[1, 1], [250, 250], [300, 250], [336, 280], \'fluid\', [320, 100], [200, 200], [300, 600], [320, 50], [300, 50], [300, 100]], \'P1\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
   gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P2\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P2\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
   gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P3\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P3\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
   gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P4\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P4\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
   gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InText_1\', [[300, 50], \'fluid\', [320, 100], [1, 1], [320, 50], [250, 250], [300, 250], [336, 280], [200, 200], [300, 100]], \'InText_1\').defineSizeMapping(mapInText).addService(googletag.pubads()));
   gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InText_2\', [[300, 50], \'fluid\', [320, 100], [1, 1], [320, 50], [250, 250], [300, 250], [336, 280], [200, 200], [300, 100]], \'InText_2\').defineSizeMapping(mapInText).addService(googletag.pubads()));
   gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InText_3\', [[300, 50], \'fluid\', [320, 100], [1, 1], [320, 50], [250, 250], [300, 250], [336, 280], [200, 200], [300, 100]], \'InText_3\').defineSizeMapping(mapInText).addService(googletag.pubads()));
   gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/Billboard_UnderArticle\', [\'fluid\', [1, 1], [300, 250], [728, 90], [336, 280]], \'Billboard_UnderArticle\').defineSizeMapping(mapUnderText).addService(googletag.pubads()));
   googletag.pubads().setTargeting("section", "%s");
   googletag.pubads().setTargeting("kwrds", "articles");
   googletag.pubads().enableSingleRequest();
   //googletag.pubads().disableInitialLoad();
   googletag.pubads().collapseEmptyDivs(true);
   googletag.enableServices();
});';

    $singleScriptMobile = 'googletag.cmd.push(function() {
   var mapBillboard = googletag.sizeMapping()
       .addSize([1024, 0], [[970,90],[970,250],\'fluid\', [970,400],[728, 90],[990,250]])
       .addSize([768, 0],  [[728, 90],\'fluid\', [468, 60]])
       .addSize([0, 0],    [[320, 100],[320, 50], [300, 50],[300, 100]])
       .build();
   var mapBoxes = googletag.sizeMapping()
       .addSize([1024, 0], [[300, 600],\'fluid\',[300, 250],[336, 280], [1,1]])
       .addSize([768, 0],  [[160, 600],\'fluid\',[300, 250],[336, 280], [1,1]])
       .addSize([0, 0],    [[320, 100],\'fluid\', [320, 50], [300, 50], [200, 200], [250, 250], [300, 100], [300, 250], [336, 280], [1,1]])
       .build();
   var mapInText = googletag.sizeMapping()
   .addSize([1024, 0], [\'fluid\',[300, 250],[336, 280], [1,1]])
   .addSize([768, 0],  [\'fluid\',[300, 250],[336, 280], [1,1]])
   .addSize([0, 0],    [[320, 100],\'fluid\', [320, 50], [300, 50], [200, 200], [250, 250], [300, 100], [300, 250], [336, 280], [1,1]])
   .build();

       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/Billboard\', [[728, 90], \'fluid\', [300, 100], [990,250], [970, 90], [468, 60], [970, 400], [970, 250], [320, 50], [300, 50], [320, 100]], \'Billboard\').defineSizeMapping(mapBillboard).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P1\',[[1, 1], [250, 250], [300, 250], [336, 280], \'fluid\', [320, 100], [200, 200], [300, 600], [320, 50], [300, 50], [300, 100]], \'P1\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P2\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P2\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P3\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P3\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P4\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P4\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InText_1\', [[300, 50], \'fluid\', [320, 100], [1, 1], [320, 50], [250, 250], [300, 250], [336, 280], [200, 200], [300, 100]], \'InText_1\').defineSizeMapping(mapInText).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InText_2\', [[300, 50], \'fluid\', [320, 100], [1, 1], [320, 50], [250, 250], [300, 250], [336, 280], [200, 200], [300, 100]], \'InText_2\').defineSizeMapping(mapInText).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InText_3\', [[300, 50], \'fluid\', [320, 100], [1, 1], [320, 50], [250, 250], [300, 250], [336, 280], [200, 200], [300, 100]], \'InText_3\').defineSizeMapping(mapInText).addService(googletag.pubads()));
    
       googletag.pubads().setTargeting("section", "%s");
       googletag.pubads().setTargeting("kwrds", "articles");
       googletag.pubads().enableSingleRequest();
       //googletag.pubads().disableInitialLoad();
       googletag.pubads().collapseEmptyDivs(true);
       googletag.enableServices();
   });';
    $singleScriptApp = 'googletag.cmd.push(function() {
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/Billboard\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'Billboard\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/P1\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'P1\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/P2\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'P2\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/P3\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'P3\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/P4\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'P4\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/InText_1\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'InText_1\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/InText_2\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'InText_2\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/InText_3\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'InText_3\').addService(googletag.pubads()));
    googletag.pubads().setTargeting("section", "%s");
    googletag.pubads().setTargeting("kwrds", "article");
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs(true);
    googletag.enableServices();
});';

    $homeScript = 'googletag.cmd.push(function() {
var mapBillboard = googletag.sizeMapping()
    .addSize([1024, 0], [[970,90],[970,250],\'fluid\', [970,400],[728, 90],[990,250]])
    .addSize([768, 0],  [[728, 90],\'fluid\', [468, 60]])
    .addSize([0, 0],    [[320, 100],[320, 50], [300, 50],[300, 100]])
    .build();
var mapBoxes = googletag.sizeMapping()
    .addSize([1024, 0], [[300, 600],\'fluid\',[300, 250],[336, 280], [1,1]])
    .addSize([768, 0],  [[160, 600],\'fluid\',[300, 250],[336, 280], [1,1]])
    .addSize([0, 0],    [[320, 100],\'fluid\',[320, 50],[300, 50],[300, 100],[300, 250],[336, 280], [1,1]])
    .build();
var mapInFeed = googletag.sizeMapping()
    .addSize([1024, 0], [[1, 1], [970, 250], [970, 90], [468, 60], [950, 90], [980, 90], [728, 90], \'fluid\', [970, 66], [960, 90]])
    .addSize([768, 0],  [[468, 60], [728, 90], [1, 1],\'fluid\'])
    .addSize([0, 0],    [[1, 1]])
    .build();
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/Billboard\', [[728, 90], \'fluid\', [300, 100], [990,250], [970, 90], [468, 60], [970, 400], [970, 250], [320, 50], [300, 50], [320, 100]], \'Billboard\').defineSizeMapping(mapBillboard).addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P1\',[[1, 1], [250, 250], [300, 250], [336, 280], \'fluid\', [320, 100], [200, 200], [300, 600], [320, 50], [300, 50], [300, 100]], \'P1\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P2\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P2\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P3\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P3\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P4\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P4\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InFeed_1\', [[1, 1], [970, 250], [970, 90], [468, 60], [950, 90], [980, 90], [728, 90], \'fluid\', [970, 66], [960, 90]], \'InFeed_1\').defineSizeMapping(mapInFeed).addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InFeed_2\', [[1, 1], [970, 250], [970, 90], [468, 60], [950, 90], [980, 90], [728, 90], \'fluid\', [970, 66], [960, 90]], \'InFeed_2\').defineSizeMapping(mapInFeed).addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InFeed_3\', [[1, 1], [970, 250], [970, 90], [468, 60], [950, 90], [980, 90], [728, 90], \'fluid\', [970, 66], [960, 90]], \'InFeed_3\').defineSizeMapping(mapInFeed).addService(googletag.pubads()));
    googletag.pubads().setTargeting("section", "naslovna");
    googletag.pubads().setTargeting("kwrds", "category");
    googletag.pubads().enableSingleRequest();
    //googletag.pubads().disableInitialLoad();
    googletag.pubads().collapseEmptyDivs(true);
    googletag.enableServices();
});';
    $homeScriptMobile = ' googletag.cmd.push(function() {
   var mapBillboard = googletag.sizeMapping()
       .addSize([1024, 0], [[970,90],[970,250],\'fluid\', [970,400],[728, 90],[990,250]])
       .addSize([768, 0],  [[728, 90],\'fluid\', [468, 60]])
       .addSize([0, 0],    [[320, 100],[320, 50], [300, 50],[300, 100]])
       .build();
   var mapBoxes = googletag.sizeMapping()
       .addSize([1024, 0], [[300, 600],\'fluid\',[300, 250],[336, 280], [1,1]])
       .addSize([768, 0],  [[160, 600],\'fluid\',[300, 250],[336, 280], [1,1]])
       .addSize([0, 0],    [[320, 100],\'fluid\',[320, 50],[300, 50],[300, 100],[300, 250],[336, 280], [1,1]])
       .build();
   var mapInFeed = googletag.sizeMapping()
       .addSize([1024, 0], [[1, 1], [970, 250], [970, 90], [468, 60], [950, 90], [980, 90], [728, 90], \'fluid\', [970, 66], [960, 90]])
       .addSize([768, 0],  [[468, 60], [728, 90], [1, 1],\'fluid\'])
       .addSize([0, 0],    [[1, 1]])
       .build();

       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/Billboard\', [[728, 90], \'fluid\', [300, 100], [990,250], [970, 90], [468, 60], [970, 400], [970, 250], [320, 50], [300, 50], [320, 100]], \'Billboard\').defineSizeMapping(mapBillboard).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P1\',[[1, 1], [250, 250], [300, 250], [336, 280], \'fluid\', [320, 100], [200, 200], [300, 600], [320, 50], [300, 50], [300, 100]], \'P1\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P2\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P2\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P3\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P3\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P4\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P4\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InFeed_1\', [[1, 1], [970, 250], [970, 90], [468, 60], [950, 90], [980, 90], [728, 90], \'fluid\', [970, 66], [960, 90]], \'InFeed_1\').defineSizeMapping(mapInFeed).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InFeed_2\', [[1, 1], [970, 250], [970, 90], [468, 60], [950, 90], [980, 90], [728, 90], \'fluid\', [970, 66], [960, 90]], \'InFeed_2\').defineSizeMapping(mapInFeed).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InFeed_3\', [[1, 1], [970, 250], [970, 90], [468, 60], [950, 90], [980, 90], [728, 90], \'fluid\', [970, 66], [960, 90]], \'InFeed_3\').defineSizeMapping(mapInFeed).addService(googletag.pubads()));
       
       googletag.pubads().setTargeting("section", "naslovna");
       googletag.pubads().setTargeting("kwrds", "category");
       googletag.pubads().enableSingleRequest();
       //googletag.pubads().disableInitialLoad();
       googletag.pubads().collapseEmptyDivs(true);
       googletag.enableServices();
   });';

//    if ($path === 'politika') {
//        $homeScriptMobile .= 'googletag.pubads().setTargeting("device", "mobile");';
//    }

    $homeScriptApp = 'googletag.cmd.push(function() {
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/Billboard\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'Billboard\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/P1\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'P1\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/P2\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'P2\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/P3\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'P3\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/P4\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'P4\').addService(googletag.pubads()));

    googletag.pubads().setTargeting("section", "naslovna");
    googletag.pubads().setTargeting("kwrds", "category");
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs(true);
    googletag.enableServices();
});';

    $categoryScript = 'googletag.cmd.push(function() {
var mapBillboard = googletag.sizeMapping()
  .addSize([1024, 0], [[970,90],[970,250],\'fluid\', [970,400],[728, 90],[990,250]])
  .addSize([768, 0],  [[728, 90],\'fluid\', [468, 60]])
  .addSize([0, 0],    [[320, 100],[320, 50], [300, 50],[300, 100]])
  .build();
var mapBoxes = googletag.sizeMapping()
  .addSize([1024, 0], [[300, 600],\'fluid\',[300, 250],[336, 280], [1,1]])
  .addSize([768, 0],  [[160, 600],\'fluid\',[300, 250],[336, 280], [1,1]])
  .addSize([0, 0],    [[320, 100],\'fluid\',[320, 50],[300, 50],[300, 100],[300, 250],[336, 280], [1,1]])
  .build();
var mapInFeed = googletag.sizeMapping()
  .addSize([1024, 0], [[1, 1], [468, 60], [728, 90], \'fluid\'])
  .addSize([768, 0],  [[468, 60], [728, 90], [1, 1],\'fluid\'])
  .addSize([0, 0],    [[1, 1]])
  .build();

  gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/Billboard\', [[728, 90], \'fluid\', [300, 100], [990,250], [970, 90], [468, 60], [970, 400], [970, 250], [320, 50], [300, 50], [320, 100]], \'Billboard\').defineSizeMapping(mapBillboard).addService(googletag.pubads()));
  gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P1\',[[1, 1], [250, 250], [300, 250], [336, 280], \'fluid\', [320, 100], [200, 200], [300, 600], [320, 50], [300, 50], [300, 100]], \'P1\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
  gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P2\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P2\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
  gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P3\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P3\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
  gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P4\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P4\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
  gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InFeed_1\', [[1, 1],[468, 60], [728, 90], \'fluid\'], \'InFeed_1\').defineSizeMapping(mapInFeed).addService(googletag.pubads()));
  gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InFeed_2\', [[1, 1],[468, 60], [728, 90], \'fluid\'], \'InFeed_2\').defineSizeMapping(mapInFeed).addService(googletag.pubads()));
  gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/InFeed_3\', [[1, 1],[468, 60], [728, 90], \'fluid\'], \'InFeed_3\').defineSizeMapping(mapInFeed).addService(googletag.pubads()));
  
  googletag.pubads().setTargeting("section", "%s");
  googletag.pubads().setTargeting("kwrds", "category");
  googletag.pubads().enableSingleRequest();
  //googletag.pubads().disableInitialLoad();
  googletag.pubads().collapseEmptyDivs(true);
  googletag.enableServices(); // desk
});';

    $categoryScriptMobile = ' googletag.cmd.push(function() {
   var mapBillboard = googletag.sizeMapping()
       .addSize([1024, 0], [[970,90],[970,250],\'fluid\', [970,400],[728, 90],[990,250]])
       .addSize([768, 0],  [[728, 90],\'fluid\', [468, 60]])
       .addSize([0, 0],    [[320, 100],[320, 50], [300, 50],[300, 100]])
       .build();
   
   var mapBoxes = googletag.sizeMapping()
       .addSize([1024, 0], [[300, 600],\'fluid\',[300, 250],[336, 280], [1,1]])
       .addSize([768, 0],  [[160, 600],\'fluid\',[300, 250],[336, 280], [1,1]])
       .addSize([0, 0],    [[320, 100],\'fluid\',[320, 50],[300, 50],[300, 100],[300, 250],[336, 280], [1,1]])
       .build();

       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/Billboard\', [[728, 90], \'fluid\', [300, 100], [990,250], [970, 90], [468, 60], [970, 400], [970, 250], [320, 50], [300, 50], [320, 100]], \'Billboard\').defineSizeMapping(mapBillboard).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P1\',[[1, 1], [250, 250], [300, 250], [336, 280], \'fluid\', [320, 100], [200, 200], [300, 600], [320, 50], [300, 50], [300, 100]], \'P1\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P2\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P2\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P3\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P3\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
       gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com/P4\', [[300, 50], \'fluid\', [320, 480], [320, 100], [1, 1], [320, 50], [250, 250], [160, 600], [300, 250], [336, 280], [300, 600], [200, 200], [300, 100]], \'P4\').defineSizeMapping(mapBoxes).addService(googletag.pubads()));
       googletag.pubads().setTargeting("section", "%s");
       googletag.pubads().setTargeting("kwrds", "category");';

    if ($path === 'politika') {
        $categoryScriptMobile .= 'googletag.pubads().setTargeting("device", "mobile");';
    }

    $categoryScriptMobile .= 'googletag.pubads().enableSingleRequest();
       //googletag.pubads().disableInitialLoad();
       googletag.pubads().collapseEmptyDivs(true);
       googletag.enableServices(); // mobile
   });';

    $categoryScriptApp = 'googletag.cmd.push(function() {
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/Billboard\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'Billboard\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/P1\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'P1\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/P2\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'P2\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/P3\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'P3\').addService(googletag.pubads()));
    gptadslots.push(googletag.defineSlot(\'/22022651321/Srpskainfo.com_Apps/P4\', [[300, 250], [320, 100], [300, 100], [300, 50], [320, 50]], \'P4\').addService(googletag.pubads()));

    googletag.pubads().setTargeting("section", "%s");
    googletag.pubads().setTargeting("kwrds", "category");
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs(true);
    googletag.enableServices();
});';

//    var_dump($path);
//    die();

    switch ( $path ) {
        case 'svijet':
        case 'drustvo':
        case 'politika':
        case 'biznis':
        case 'hronika':
        case 'sport':
        case 'zabava':
        case 'kultura':
        case 'kolumne':
        case 'banjaluka':
        case 'regioni': // renamed to gradovi
        case 'gradovi':
        case 'sve-vijesti':
        case 'popularni-clanci-app':
        case 'popularno':
        case 'novi-clanici-app':
        case 'novo':
        case 'digitalna-srpska':
        if ($isMobileApp) {
            $script = $categoryScriptApp;
        } elseif (wp_is_mobile()) {
            $script = $categoryScriptMobile;
        } else {
            $script = $categoryScript;
        }
//            var_dump(sprintf( $script, $path ));
//            die();

            return sprintf( $script, $path );
            break;

        case 'tag':
        case 'author':
        case 'pretraga':
            // App $_GET['type'] is 'search', on desk it is 'pretraga'
        case 'search':
        if ($isMobileApp) {
            $script = $categoryScriptApp;
        } elseif (wp_is_mobile()) {
            $script = $categoryScriptMobile;
        } else {
            $script = $categoryScript;
        }

            return sprintf( $script, 'ostalo' );
            break;

        case '':
        case 'mobile-home':
            if ($isMobileApp) {
                $script = $homeScriptApp;
            } elseif (wp_is_mobile()) {
                $script = $homeScriptMobile;
            } else {
                $script = $homeScript;
            }

            return $script;
            break;

        default:
            // @TODO solve better. single page.

            $queriedObjectId = (int) get_queried_object_id();
            if (!$queriedObjectId || $queriedObjectId === 747407 // prod page
            || $queriedObjectId === 713038 // dev page
            ) {
                $appSinglePost = get_posts([
                    'name'        => $_GET['url'],
                    'post_type'   => 'post',
                    'post_status' => 'publish',
                    'numberposts' => 1
                ])[0];
                $queriedObjectId = $appSinglePost->ID;
            }

            $categoryName = strtolower( get_category( wp_get_post_categories( $queriedObjectId )[0] )->name );
            if ($isMobileApp) {
                $script = $singleScriptApp;
            } elseif (wp_is_mobile()) {
                $script = $singleScriptMobile;
            } else {
                $script = $singleScript;
            }

            return sprintf( $script, $categoryName );
            break;
    }
}


