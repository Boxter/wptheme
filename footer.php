<?php
// Check if request is coming from an APP and set route param in var
global $isApp;

if ($isApp) {
    include("templates/footer/view/footerMobile.php");
} else {
    include("templates/footer/view/footer.php");
}
wp_footer();?>
</body>
</html>
